        <div class="footer"> 
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <img src="<?php bloginfo('template_url');?>/landing/images/logo_udp.png">
                        <img src="<?php bloginfo('template_url');?>/landing/images/logo_uah.png">
                        <img src="<?php bloginfo('template_url');?>/landing/images/logo_uct.png">
                        <img src="<?php bloginfo('template_url');?>/landing/images/logo_ut.png">
                        <img src="<?php bloginfo('template_url');?>/landing/images/logo_chile.png">
                    </div> 
                    <div class="clearfix"></div>
                    <div class="col-sm-6 text-left">
                        <p>Información legal ©   todos los derechos reservados.</p>
                    </div>
                    <div class="col-sm-6 text-right">
                    </div>
                </div>
             </div>
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="<?php bloginfo('template_url');?>/landing/js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="<?php bloginfo('template_url');?>/landing/js/bootstrap.min.js"></script> 
        <script src="<?php bloginfo('template_url');?>/landing/js/scrollreveal.min.js"></script> 
        <script src="<?php bloginfo('template_url');?>/landing/js/landing.js"></script> 

        <?php	wp_footer(); ?>
    </body>
</html>
