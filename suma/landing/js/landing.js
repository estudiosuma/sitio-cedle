$(document).ready(function() { 
    $('#content-msj-exito').hide();
    $(".error").hide();
    $(".bien").hide();
    // Menu Toggle mobile 
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#menu").toggleClass("toggled");
    }); 

    //Anclas menu
    $('.sidebar-navs a, #seccion_01 a').click(function(){
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
        return false;
    });

    //ciudades
    $("#santiago").show();
    $('.ciudades a').click(function(){
        $('.ciudades a').removeClass('activo');
        $(this).addClass('activo');
        var ciudad = $.attr(this, 'href');
        $('.itemciudad').hide();
        $(ciudad).show();

        return false;
    });

    //animacion scroll
    window.sr = ScrollReveal();
    sr.reveal('#seccion_02', { duration: 1000 });
    sr.reveal('#seccion_03', { duration: 1000 });
    sr.reveal('#seccion_04 .body', { duration: 1000 }); 
     

    $( "#btn-inscripcion" ).click(function() {
        $(".error").hide();
        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        var nombre = $.trim($("#nombre").val());
        var rut = $.trim($("#rut").val());
        var cargo = $.trim($("#cargo").val());
        var institucion = $.trim($("#institucion").val());
        var email = $.trim($("#correo").val()); 
        var ciudad = document.querySelector('input[name="ciudad"]:checked').value;

        if ( nombre == 0) {  $("#nombre").next().fadeIn(); return false; }; 
        if ( rut == 0) {  $("#rut").next().fadeIn(); return false; }; 
        if ( cargo == 0) {  $("#cargo").next().fadeIn(); return false; }; 
        if ( institucion == 0) {  $("#institucion").next().fadeIn(); return false; }; 
        if (!regex.test(email)) { $("#correo").next().fadeIn(); return false;}  
        
        // alert('Todo ok! Se inicia ajax.');
        $.ajax({
               url: "/wp-content/themes/suma/ajax/landing.php",  
               type: "POST",
               data: {
                        nombre:nombre,
                        rut:rut,
                        cargo:cargo,
                        institucion:institucion,
                        email:email,
                        ciudad:ciudad
                },
               async: true,
               cache: false,
               success:function(response,textStatus,xhr,data,callback,result){
                   $('#id-formulario-landing').hide();
                    console.log(response);
                    if (response=='bien') {
                        $('#content-msj-exito').show();
                        $(".error").hide();
                        $(".bien").show();
                    }else{
                        $('#content-msj-exito').show();
                        $(".error").show();
                        $(".bien").hide();
                    }
                    setTimeout(function(){
                        $("#nombre").val(null);
                        $("#rut").val(null);
                        $("#cargo").val(null);
                        $("#institucion").val(null);
                        $("#correo").val(null);
                        $('#content-msj-exito').hide();
                        $(".error").hide();
                        $(".bien").hide();
                        $('#id-formulario-landing').show();
                        window.location.hash = '#id-formulario-landing';
                    },5000);
                   // console.log(response);
               },
              error: function(){
            }
        });
        return false
    });

    

});