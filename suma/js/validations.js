$(document).ready(function() { 
    
        $( "#inscribir-newsletter" ).click(function() {
            
        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        var email = $.trim($("#correo-newsletter").val());
            
        if (!regex.test(email)) { 
            $(".error").css('display', 'block');
            $("#correo-newsletter").prev().fadeIn();
            return false;
        }else{
            $('#modal-bienvenido').modal('show');
            $('#modal-bienvenido h1').html('<h1>Bienvenido/A <strong>'+ email +'</strong></h1>');
        };
            
            $( "#btn-confirmar-bienvenido" ).click(function() {
                $(".error").hide();
                var cargo = $("form input[type='radio']:checked").val();

                    $.ajax({
                           url: "/wp-content/themes/suma/ajax/mailchimp/api.php",  
                           type: "POST",
                           data: {
                                email:email,
                                cargo:cargo,
                            },
                           async: true,
                           cache: false,
                           success:function(response,textStatus,xhr,data,callback,result){
                               // $('.modal-body').html('<div class="row"><div class="col-xs-12 text-center"></div><div class="col-xs-12"><p>SE HA COMPLETADO TU INSCRIPCIÓN.</p><br><p>POR FAVOR REVISA TU BANDEJA DE ENTRADA PARA CONFIRMAR TU SUSCRIPCIÓN.</p></div></div>');
                              $('#modal-bienvenido').modal('hide');  
                                $('#modal-exito').modal('show'); 
                           },
                          error: function(){
                              // $('.modal-body').html('<div class="row"><div class="col-xs-12 text-center"></div><div class="col-xs-12"><p>HA OCURRIDO UN ERROR EN TU INSCRIPCIÓN.</p></div></div>');
                        }
                    });

                return false
            });
    });
    
        $( "#btn-enviar-suscripcion" ).click(function() {
            
        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        var email = $.trim($("#correo-newsletter-index").val());
            
        if (!regex.test(email)) { 
            $(".error").css('display', 'block');
            $("#correo-newsletter-index").prev().fadeIn();
            return false;
        }else{
            $('#modal-bienvenido').modal('show');
            $('#modal-bienvenido h1').html('<h1>Bienvenido/A <strong>'+ email +'</strong></h1>');
        };
            
            $( "#btn-confirmar-bienvenido" ).click(function() {
                $(".error").hide();
                var cargo = $("form input[type='radio']:checked").val();

                    $.ajax({
                           url: "/wp-content/themes/suma/ajax/mailchimp/api.php",  
                           type: "POST",
                           data: {
                                email:email,
                                cargo:cargo,
                            },
                           async: true,
                           cache: false,
                           success:function(response,textStatus,xhr,data,callback,result){
                               $('.modal-body').html('<div class="row"><div class="col-xs-12 text-center"></div><div class="col-xs-12"><p>SE HA COMPLETADO TU INSCRIPCIÓN.</p><br><p>POR FAVOR REVISA TU BANDEJA DE ENTRADA PARA CONFIRMAR TU SUSCRIPCIÓN.</p></div></div>');
                           },
                          error: function(){
                              $('.modal-body').html('<div class="row"><div class="col-xs-12 text-center"></div><div class="col-xs-12"><p>HA OCURRIDO UN ERROR EN TU INSCRIPCIÓN.</p></div></div>');
                        }
                    });

                return false
            });
    });

    $( "#btn-inscripcion" ).click(function() {
        $(".error").hide();
        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        var nombre = $.trim($("#inputnombre").val());
        var correo = $.trim($("#inputcorreo").val());
        var asunto = $.trim($("#inputasunto").val());
        var mensaje = $.trim($("#inputmensaje").val());

        if ( nombre == 0) {  $("#inputnombre").next().fadeIn(); return false; }; 
        if (!regex.test(correo)) { $("#inputcorreo").next().fadeIn(); return false;}
        if ( asunto == 0) {  $("#inputasunto").next().fadeIn(); return false; }; 
        if ( mensaje == 0) {  $("#inputmensaje").next().fadeIn(); return false; }; 
        
        $('#modal-inscripcion').modal('hide');
        
            $.ajax({
                   url: "/wp-content/themes/suma/ajax/inscribete.php",  
                   type: "POST",
                   data: {
                        nombre :nombre,
                        correo: correo,
                        asunto: asunto,
                        mensaje: mensaje,
                    },
                   async: true,
                   cache: false,
                   success:function(response,textStatus,xhr,data,callback,result){
                       $('#modal-exito').modal('show');
                   },
                  error: function(){
                      $('#modal-error').modal('show');
                }
            });

        return false
    });

    $( "#btn-contacto" ).click(function() {
        $(".error").hide();
        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        var nombre = $.trim($("#inputnombre").val());
        var correo = $.trim($("#inputcorreo").val());
        var cargo = $.trim($("#selectcargo").val());
        var asunto = $.trim($("#inputasunto").val());
        var mensaje = $.trim($("#inputmensaje").val());

        if ( nombre == 0) {  $("#inputnombre").next().fadeIn(); return false; }; 
        if (!regex.test(correo)) { $("#inputcorreo").next().fadeIn(); return false;};
        if ( asunto == 0) {  $("#inputasunto").next().fadeIn(); return false; }; 
        if ( mensaje == 0) {  $("#inputmensaje").next().fadeIn(); return false; }; 
        if ( cargo == 0) {  $("#selectcargo").next().fadeIn(); return false; }; 
        
            $.ajax({
                   url: "/wp-content/themes/suma/ajax/contacto.php",  
                   type: "POST",
                   data: {
                        nombre :nombre,
                        correo: correo,
                        cargo : cargo,
                        asunto: asunto,
                        mensaje: mensaje,
	                },
	               async: true,
	               cache: false,
	               success:function(response,textStatus,xhr,data,callback,result){
	                   $('#id-formulario-contacto').html('<div><label>Su Mensaje de contacto ha sido enviado con exito <br> Pronto nos comunicaremos</label></div>');
	               },
	              error: function(){
                      $('#id-formulario-contacto').html('<div><label>Ha ocurrido un error al enviar su mensaje <br> Por favor intente de nuevo</label></div>');
	            }
            });

        return false
    });


});