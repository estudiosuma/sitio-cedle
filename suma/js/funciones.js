
    





$.fn.scrollTo = function( target, options, callback ){
  if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
  var settings = $.extend({
    scrollTarget  : target,
    offsetTop     : 50,
    duration      : 500,
    easing        : 'swing'
  }, options);
  return this.each(function(){
    var scrollPane = $(this);
    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
      if (typeof callback == 'function') { callback.call(this); }
    });
  });
}


$( window ).load(function() {



	$('#carousel-1').carousel('pause');
	$('#carousel-2').carousel();

	
	/*$( ".btn-enviar, .agregar" ).click(function() {
		 $('#modal-bienvenido').modal('show');  
	  	return false
	});
	$( "#btn-confirmar-bienvenido" ).click(function() {
		 $('#modal-bienvenido').modal('hide');  
	  	return false
	});*/

	//modal incribirse
	$( ".btn-inscribirme" ).click(function() {
		 $('#modal-inscripcion').modal('show'); 

	  	return false
	});
	
	//select
	if ($('.selectpicker').length > 0) { 
		/*select*/
		$('.selectpicker').selectpicker({ 
		  size: 7
		});
		$('.anios').selectpicker({ 
		  size: 7
		});


		$('.section_agenda').on('change','#agenda-mes, #agenda-ano',function(){
			console.log($(this).val());
			// alert('Todo ok! Se inicia ajax.');
			var ano_agenda= $('#agenda-ano').val();
			var mes_agenda= $('#agenda-mes').val();
			$('.sections-8').html('<div class="rows"><div class="col-md-12 " style="text-align: center;"><img  src="/wp-content/themes/suma/img/728.gif"></div></div>');
	        $.ajax({
	               url: "/wp-content/themes/suma/ajax/agenda.php",  
	               type: "POST",
	               data: {
	                        ano_agenda:ano_agenda,
	                        mes_agenda:mes_agenda
	                },
	               async: true,
	               cache: false,
	               success:function(response,textStatus,xhr,data,callback,result){
	                   $('.sections-8').html(response);
	                   // console.log(response);
	               },
	              error: function(){
	            }
	        });
		});


	}

	//animacion scroll
	window.sr = ScrollReveal();
	sr.reveal('.section-2', { duration: 1000 });
	//sr.reveal('.section-3 ', { duration: 1000 });
	sr.reveal('.section-4 .box-noticia', { duration: 1000 });
	sr.reveal('.section-5', { duration: 1000 });
	sr.reveal('.section-6 .box-publicaciones', { duration: 1000 }); 
	sr.reveal('.section-9 .box', { duration: 1000 }); 

	/* menu lateral */
		var altopantalla = $(window).height();
		var anchopadre = $("#padre-side").width();
		$('.menu-lateral').height(altopantalla); 
		$('.menu-lateral').width(anchopadre +30);  
		$('.page-template-index .menu-lateral').width(anchopadre +30);  
		$( window ).resize(function() {
			var altopantalla = $(window).height();
			var anchopadre = $("#padre-side").width();
		  	$('.menu-lateral').height(altopantalla); 
			$('.menu-lateral').width(anchopadre +30);  
			$('.page-template-index .menu-lateral').width(anchopadre +30);  
		});

		$(window).scroll(function(e){ 
			$('.menu-lateral').css({
				 position: 'fixed'
	        }); 
	    }); 
     

	/* buscador */ 
		$( ".menu-lateral .box-buscador .box" ).hover(
		  function() {
		     $('.menu-lateral .option').stop().slideDown();
		  }, function() {
		    $('.menu-lateral .option').stop().slideUp();
		  }
		);

	/* scroll */
	$(".linea-tiempo").mCustomScrollbar({
		scrollButtons:{enable:true},
		theme:"light-thick",
		scrollInertia:100,
		scrollbarPosition:"outside"
	}); 
	$(".scroll").mCustomScrollbar({
		scrollButtons:{enable:true},
		theme:"light-thick",
		scrollInertia:100,
		scrollbarPosition:"inside"
	});

	/* evento */
	var x = 2;
	$( ".calendario .activo" ).click(function() {
		/* contenido simulado */
		var evento = $('#evento-'+$(this).data('evento')).html();
		console.log($(this).data('evento'));

		/* fin contenido simulado */
	  	$('.item-evento').fadeOut( "slow", function() {
		    $('.item-evento').html(evento);
		    $('.item-evento .txt').addClass("scroll");
		     $('.item-evento .txt').data( "mcs-theme",'minimal-dark' );
		     $(".scroll").mCustomScrollbar({
                        scrollButtons:{enable:true},
                        theme:"light-thick",
                        scrollInertia:100,
                        scrollbarPosition:"inside"
                    });
		  	$('.item-evento').fadeIn();
	  	});
	  	x++;

	  	var evento_id = $(this).data('evento');
        setTimeout(function(){
        $('.item-evento .txt .col-md-12').addClass("redes-evento-"+evento_id );
	        $.ajax({
	               url: "/wp-content/themes/suma/ajax/redes_evento.php",  
	               type: "POST",
	               data: {
	                        id:evento_id 
	                },
	               async: true,
	               cache: false,
	               success:function(response,textStatus,xhr,data,callback,result){
	                    console.log($('.item-evento .txt .redes-evento-'+evento_id ).length);
	                   $('.item-evento .txt .redes-evento-'+evento_id ).html(response);
	               },
	              error: function(){
	            }
	        });
        },1000);
	  	return false
	});
	$( ".btn_mapa" ).click(function() { 

		/* contenido simulado */
		if ( $( this ).hasClass( "mapa_california" ) ) {
			var sucursal =$('#div-contenedor-mapa_california').html();
		}
		if ( $( this ).hasClass( "mapa_santiago" ) ) {
			var sucursal =$('#div-contenedor-mapa_santiago').html();
		}
		if ( $( this ).hasClass( "mapa_talca" ) ) {
			var sucursal =$('#div-contenedor-mapa_talca').html();
		}
		if ( $( this ).hasClass( "mapa_temuco" ) ) {
			var sucursal =$('#div-contenedor-mapa_temuco').html();
		}
			
		/* fin contenido simulado */

		$(".btn_mapa").removeClass('active');
		$(this).addClass('active');
	  	$('.contenido_sucursal').fadeOut( "slow", function() { 
		    $('.contenido_sucursal').html(sucursal);
		  	$('.contenido_sucursal').fadeIn();
	  	}); 
	  	return false
	});

	//letras animadas
	if ($(".element").length > 0) {
		var letras_animadas = $(".element").html().toString();
		var nuevas_letras_animadas= letras_animadas.split("-");
	}else{
		var nuevas_letras_animadas= '';
	}
	
	$(".element").typed({
        strings: nuevas_letras_animadas,
        typeSpeed: 100,
        backSpeed: 100,
         backDelay: 1000,
         loop: true
      });

	// Menu Toggle mobile 
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $(".menu-lateral").toggleClass("toggled");
    }); 

    //autocomplete
   // var personas = [{"value":"A. Cerda", "data":"Encargada De Producción Y Extensión", "descripcion":"", "email":""},{"value":"Alma Harris", "data":"Encargada De Producción Y Extensión", "descripcion":"", "email":""},{"value":"Andrea Horn Kupfer", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Bernardita Justiniano", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Bert Creemers", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Boudewijn van Velzen", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"C. Frites", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"C. Gajardo", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Carlos Beca", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Carlos Concha", "data":"FormaciÓn Y Desarrollo De Estrategias Formativas", "descripcion":"Magister en Educación. Administración Educacional", "email":"cconcha@uahurtado.cl"},{"value":"Carmen Sotomayor", "data":"FormaciÓn Y Desarrollo De Estrategias Formativas", "descripcion":"", "email":""},{"value":"Carol Campbell", "data":"FormaciÓn Y Desarrollo De Estrategias Formativas", "descripcion":"", "email":""},{"value":"Christopher Chapman", "data":"FormaciÓn Y Desarrollo De Estrategias Formativas", "descripcion":"", "email":""},{"value":"Claudio Almonacid", "data":"FormaciÓn Y Desarrollo De Estrategias Formativas", "descripcion":"", "email":""},{"value":"Cristian Cox", "data":"Investigación Y Políticas Públicas", "descripcion":"Doctor en Sociología", "email":"ccoxdonoso@gmail.com"},{"value":"Cristobal Villalobos", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Dagmar Raczynski", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Daniel Muijs", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Daniela Ortega", "data":"Coordinadora De Análisis Institucional", "descripcion":"Socióloga", "email":"daniela.ortega@udp.cl"},{"value":"David Durán", "data":"Coordinadora De Análisis Institucional", "descripcion":"", "email":""},{"value":"David Reynolds", "data":"Coordinadora De Análisis Institucional", "descripcion":"", "email":""},{"value":"Elisabeth Merino", "data":"Coordinadora De Análisis Institucional", "descripcion":"", "email":""},{"value":"Ester Miquel", "data":"Coordinadora De Análisis Institucional", "descripcion":"", "email":""},{"value":"Fernando Maureira Tapia", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Francisca Zamorano", "data":"Asistente De Dirección", "descripcion":"Cientista Politico", "email":"francisca.zamorano@udp.cl"},{"value":"G. Davis", "data":"Asistente De Dirección", "descripcion":"", "email":""},{"value":"Gonzalo Munoz", "data":"Asistente De Dirección", "descripcion":"", "email":""},{"value":"Heinrich Mintrop", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Isabel Castro", "data":"Encargada De Operaciones", "descripcion":"Ingeniero Comercial", "email":"isabel.castro@udp.cl"},{"value":"Javier San Miguel", "data":"Encargada De Operaciones", "descripcion":"", "email":""},{"value":"Jorge Alarcón", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"José Weinstein", "data":"Director", "descripcion":"Doctor en Sociología", "email":"jose.weinstein@udp.cl"},{"value":"Juan Garcia-Huidobro", "data":"Director", "descripcion":"", "email":""},{"value":"L. García", "data":"Director", "descripcion":"", "email":""},{"value":"L. López", "data":"Director", "descripcion":"", "email":""},{"value":"Leonidas Kyriakides", "data":"Director", "descripcion":"", "email":""},{"value":"Liliana Sánchez", "data":"Director", "descripcion":"", "email":""},{"value":"Lorena Meckes", "data":"Director", "descripcion":"", "email":""},{"value":"Lorena Ramírez", "data":"Gerenta De Operaciones", "descripcion":"Trabajadora Social", "email":"lorena.ramirez@udp.cl"},{"value":"Lorna Earl", "data":"Gerenta De Operaciones", "descripcion":"", "email":""},{"value":"Louise Stoll", "data":"Gerenta De Operaciones", "descripcion":"", "email":""},{"value":"M. Castro", "data":"Gerenta De Operaciones", "descripcion":"", "email":""},{"value":"M. Sandoval", "data":"Gerenta De Operaciones", "descripcion":"", "email":""},{"value":"Macarena  Hernández", "data":"Gerenta De Operaciones", "descripcion":"", "email":""},{"value":"Maite Oller", "data":"Gerenta De Operaciones", "descripcion":"", "email":""},{"value":"Marcela Delpiano", "data":"Encargada De Producción Y Extensión", "descripcion":"Comunicadora Audiovisual", "email":"marcela.delpiano@udp.cl"},{"value":"Marcela Guzmán", "data":"Encargada De Producción Y Extensión", "descripcion":"", "email":""},{"value":"María Elena Mellado", "data":"Transformación De Las Redes De Mejoramiento Escolar Encomunidades Profesionales De Aprendizaje Para El Desarrollo De Capacidades De Liderazgo Escolar.", "descripcion":"", "email":""},{"value":"María Ester Silva", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Marianela Cerri", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Mariona Corcelles", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Marta Flores", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Marta Utset", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Martín Bascopé", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Montserrat Fons", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"N. Benavides", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Natalia Barrientos", "data":"Coordinadora De Comunicaciones", "descripcion":"Periodista", "email":"natalia.barrientos@udp.cl"},{"value":"O. Arias", "data":"Coordinadora De Comunicaciones", "descripcion":"", "email":""},{"value":"O. Corvalán", "data":"Coordinadora De Comunicaciones", "descripcion":"", "email":""},{"value":"Paulina Herrera", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Ricardo Carbone", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Rosario Rivero Castro", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"S. Sánchez", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Sam Stringfield", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Sebastián Donoso", "data":"Jefe De Proyecto", "descripcion":"Doctor en Educación", "email":"sdonoso@utalca.cl    "},{"value":"Sílvia Blanch", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"V. Cancino", "data":"Jefe De Proyecto", "descripcion":"", "email":""},{"value":"Vanessa Valdebenito", "data":"InnovaciÓn Y Desarrollo De Modelos Y PrÁcticas", "descripcion":"Doctora en Psicología de la Educación", "email":"vvaldebenito@uct.cl"}];
	var personas = $('#contenido-mimebros-autocomplete').data('miembros_autocomplete');
	console.log(personas);

	$('#autocomplete').autocomplete({
	    lookup: personas,
	    onSelect: function (suggestion) {
	        //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
	        $('.contenido_buscador .item').html("<h4>" + suggestion.value +"</h4>  <h5>" + suggestion.data +"</h5> <p>" + suggestion.descripcion +"</p> <span>" + suggestion.email +"</span>")
	    }
	});

	//buscador
	// $('#box-palabra').show();
	$('#box-directorio').show();
	 
	$(".btnbuscador").click(function(e) {
        e.preventDefault();
        var box = $(this).attr('href');
        $(".btnbuscador").parent().removeClass('active');
        $(this).parent().addClass('active');
        $('.row-buscar-equipo .items').hide();
        $(box).show();
    });

    $("#box-directorio a").click(function(e) {
        e.preventDefault(); 
        var miembro = $(this).data('info_miembro');
        console.log(miembro.name);
        $('.contenido_buscador .item').html("<h4>"+miembro.name+"</h4><h5>"+miembro.cargo+"</h5><p>"+miembro.descripcion+"</p> <span>"+miembro.email+"</span>")
    }); 
    $('#box-directorio ul li a').first().trigger('click'); 

    //alto de item de nosotros
    $( ".row-equipo .item" ).each(function( index ) {
	    var altop = $(this).find('p').height();
	    var altospan = $(this).find('span').height();
	    var total = altop + altospan + 0;
	   // console.log(altop + '---' + altospan);
	    $(this).find('.info').css("bottom","-"+total+"px"); 
	});
	$( window ).resize(function() {
		$( ".row-equipo .item" ).each(function( index ) {
	    var altop = $(this).find('p').height();
	    var altospan = $(this).find('span').height();
	    var total = altop + altospan + 0;
	    $(this).find('.info').css("bottom","-"+total+"px"); 
	});
	});


	//responsive noticias

	
    $( window ).resize(function() {
    	var ventana_ancho = $(window).width();
    	 if(ventana_ancho < 992 ){
    	 	$(window).scroll(function(e){ 
				var posi = $( window ).scrollTop();
				var alto = $(window).height();
				var fix = posi + alto + 50; 
				posicionReal = $(".box-prensa").position();
				var altoprensa = posicionReal.top;
				if(fix > altoprensa){
					$('.btn_fixed').fadeOut();
				}else{
					$('.btn_fixed').fadeIn();
				}
		    });
		     $('#modal-bienvenido').modal('hide');  
    	 }else{
    	 	$('.btn_fixed').hide();
    	 }
    	 
	});
	var ventana_ancho = $(window).width();
    	 if(ventana_ancho < 992 ){
    	 	$(window).scroll(function(e){ 
				var posi = $( window ).scrollTop();
				var alto = $(window).height();
				var fix = posi + alto + 50; 
				posicionReal = $(".box-prensa").position();
				var altoprensa = posicionReal.top;
				if(fix > altoprensa){
					$('.btn_fixed').fadeOut();
				}else{
					$('.btn_fixed').fadeIn();
				}
		    });
    	 }

    $(".btn_fixed").click(function(e) {
        e.preventDefault(); 
        posicionReal = $(".box-prensa").position();
		var altoprensa = posicionReal.top;
        $('body').scrollTo(altoprensa);
    }); 


    if ($('#grid').length > 0) { 
	 	var $grid = $('#grid'),
	 	$sizer = $grid.find('.shuffle__sizer'); 

	  	$grid.shuffle({
	    	itemSelector: '.items',
	    	sizer: $sizer
	  	});


  	$grid.shuffle( 'shuffle', 'columnas' );

 	$('.filter-options a').click(function (e) {
	  	e.preventDefault();
	  	var $this = $(this),
      	isActive = $this.hasClass( 'active' ),
      	group = isActive ? 'all' : $this.data('group');
      	if ( !isActive ) {
	        $('.filter-options .active').removeClass('active');
	  	}
  		$this.toggleClass('active');
  		$grid.shuffle( 'shuffle', group );
			   
	}) 

	}

	if ($('#grid2').length > 0) { 
	 	var $grid = $('#grid2'),
	 	$sizer = $grid.find('.shuffle__sizer'); 

	  	$grid.shuffle({
	    	itemSelector: '.items',
	    	sizer: $sizer
	  	});


	  	$grid.shuffle( 'shuffle', 'Artículos' );

	 	$('.filter-options a').click(function (e) {
		  	e.preventDefault();
		  	$('.error-publiaciones').hide();
		  	var $this = $(this),
	      	isActive = $this.hasClass( 'active' ),
	      	group = isActive ? 'all' : $this.data('group');
	      	if ( !isActive ) {
		        $('.filter-options .active').removeClass('active');
		  	}
	  		$this.toggleClass('active');
	  		$grid.shuffle( 'shuffle', group ); 
	  		console.log($('.filtered').length);
	  		if ($('.filtered').length==0) {
	  			$('.error-publiaciones').show();
	  		}
		}) 

	}
	$('.error-publiaciones').hide();
	$('.btn-search-home').click(function(){
		$('#form_search_home').submit();
	});
	$('.btn-search-page').click(function(){
		$('#form_search_page').submit();
	});

	$('.section_actualidad').on('change','#select-publicacion-ano, #select-publicacion-autor, #select-publicacion-tema',function(e){
			e.preventDefault();
			$('.error-publiaciones').hide();
			console.log($(this).val());
			var select_publicacion_ano = $('#select-publicacion-ano').val();
			var select_publicacion_autor = $('#select-publicacion-autor').val();
			var select_publicacion_tema = $('#select-publicacion-tema').val();
			var activo_clase = $('.active').data('group');
			if ($('#grid2').length > 0) { 
           		$('#grid2').shuffle('destroy');
           	}
			$('#grid2').html('<div class="rows"><div class="col-md-12 " style="text-align: center;"><img  src="/wp-content/themes/suma/img/728.gif"></div></div><div class="col-md-4 items" data-groups="all"></div> ');
	        
	        $.ajax({
	               url: "/wp-content/themes/suma/ajax/publicaciones.php",  
	               type: "POST",
	               data: {
						select_publicacion_ano: select_publicacion_ano,
						select_publicacion_autor: select_publicacion_autor,
						select_publicacion_tema: select_publicacion_tema,
						activo_clase: activo_clase
	                },
	               async: true,
	               cache: false,
	               success:function(response,textStatus,xhr,data,callback,result){
	               		
		               	// $('#contenedor-de-publicaciones').html('<div class="col-md-12 test" id="grid2"> </div>');
						$('#grid2').html(response);
	                   // console.log(response);
	                    setTimeout(function(){
						    if($('.active').data('group')=='Cuaderno'){
						        $('#btn_cuaderno').trigger('click');
						        $('#btn_cuaderno').trigger('click');
						        // $grid.shuffle( 'shuffle', $('.active').data('group') ); 
						    }
						    if($('.active').data('group')=='Artículos'){
						        $('#btn_estudios').trigger('click');
						        $('#btn_estudios').trigger('click');
						        // $grid.shuffle( 'shuffle', $('.active').data('group') ); 
						    }
						    if($('.active').data('group')=='Estudios'){
						       $('#btn_informe').trigger('click');
						       $('#btn_informe').trigger('click');
						       // $grid.shuffle( 'shuffle', $('.active').data('group') ); 
						    }
						},2000);
	               },
	              error: function(){
	            }
	        });
	        // $('#contenedor_cajas').html('<nav class="filter-options row"><a href="#" data-group="Cuaderno" id="btn_cuaderno" class=" active"> Cuadernos</a><a href="#" data-group="Artículos" id="btn_estudios"  >  Artículos</a> <a href="#" data-group="Estudios" id="btn_informe"  > Estudios</a> </nav>');
		});
	$('.section-hiden').hide();
});

