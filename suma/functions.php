<?php
	//Creacion del postype
	add_action( 'init', 'create_post_type' );
	function create_post_type(){
		register_post_type( 'noticias',
			array(
				'labels' => array(
					'name' => __( 'Noticias' ),
					'singular_name' => __( 'noticias' )
				),
			'public' => true,
			'has_archive' => true,
			'supports'           => array( 'title' )

			)
		);
		register_post_type( 'prensa',
				array(
					'labels' => array(
						'name' => __( 'Prensa' ),
						'singular_name' => __( 'prensa' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'lineas_de_trabajo',
				array(
					'labels' => array(
						'name' => __( 'Lineas de Trabajo' ),
						'singular_name' => __( 'lineas_de_trabajo' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'entrevista',
				array(
					'labels' => array(
						'name' => __( 'Entrevista' ),
						'singular_name' => __( 'entrevista' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'infografia',
				array(
					'labels' => array(
						'name' => __( 'Infografia' ),
						'singular_name' => __( 'infografia' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'columna',
				array(
					'labels' => array(
						'name' => __( 'Columna' ),
						'singular_name' => __( 'columna' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'recomendado',
				array(
					'labels' => array(
						'name' => __( 'Recomendado' ),
						'singular_name' => __( 'recomendado' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'publicaciones',
				array(
					'labels' => array(
						'name' => __( 'Publicaciones' ),
						'singular_name' => __( 'publicaciones' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'evento',
				array(
					'labels' => array(
						'name' => __( 'Eventos' ),
						'singular_name' => __( 'evento' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'sucursal',
				array(
					'labels' => array(
						'name' => __( 'Sucursales' ),
						'singular_name' => __( 'sucursal' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'banner',
				array(
					'labels' => array(
						'name' => __( 'Banners' ),
						'singular_name' => __( 'banner' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'equipo_trabajo',
				array(
					'labels' => array(
						'name' => __( 'Equipos de Trabajo' ),
						'singular_name' => __( 'equipo_trabajo' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'equipo_cedle',
				array(
					'labels' => array(
						'name' => __( 'Equipos del CEDLE' ),
						'singular_name' => __( 'equipo_cedle' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'normativa',
				array(
					'labels' => array(
						'name' => __( 'Normativas' ),
						'singular_name' => __( 'normativa' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
		register_post_type( 'newsletter',
				array(
					'labels' => array(
						'name' => __( 'Newsletters' ),
						'singular_name' => __( 'newsletter' )
					),
				'public' => true,
				'has_archive' => true,
				'supports'           => array( 'title' )

				)
			);
	}
	add_action( 'init', 'crear_categorias_noticias' );
	function crear_categorias_noticias() {
		register_taxonomy('noticias_categoria','noticias',
			array(
				'label' => __( 'Categorias de Noticias' ),
				'rewrite' => array( 'slug' => 'noticias_categoria' ),
				'hierarchical' => true,
			)
		);
	}



	add_action( 'init', 'crear_categorias_publicaciones' );
	function crear_categorias_publicaciones() {
		register_taxonomy('publicaciones_categoria','publicaciones',
			array(
				'label' => __( 'Categorias de Publicaciones' ),
				'rewrite' => array( 'slug' => 'publicaciones_categoria' ),
				'hierarchical' => true,
			)
		);
	}

	add_action( 'init', 'crear_tipos_publicaciones' );
	function crear_tipos_publicaciones() {
		register_taxonomy('publicaciones_tipo','publicaciones',
			array(
				'label' => __( 'Tipos de Publicaciones' ),
				'rewrite' => array( 'slug' => 'publicaciones_tipo' ),
				'hierarchical' => true,
			)
		);
	}

	add_action( 'init', 'crear_temas_publicaciones' );
	function crear_temas_publicaciones() {
		register_taxonomy('publicaciones_tema','publicaciones',
			array(
				'label' => __( 'Temas de Publicaciones' ),
				'rewrite' => array( 'slug' => 'publicaciones_tema' ),
				'hierarchical' => true,
			)
		);
	}

	add_action( 'init', 'crear_categorias_infografia' );
	function crear_categorias_infografia() {
		register_taxonomy('infografia_categoria','infografia',
			array(
				'label' => __( 'Categorias de Infografia' ),
				'rewrite' => array( 'slug' => 'infografia_categoria' ),
				'hierarchical' => true,
			)
		);
	}

	add_action( 'init', 'crear_categorias_columna' );
	function crear_categorias_columna() {
		register_taxonomy('columna_categoria','columna',
			array(
				'label' => __( 'Categorias de Columna' ),
				'rewrite' => array( 'slug' => 'columna_categoria' ),
				'hierarchical' => true,
			)
		);
	}

	add_action( 'init', 'crear_categorias_prensa' );
	function crear_categorias_prensa() {
		register_taxonomy('prensa_categoria','prensa',
			array(
				'label' => __( 'Categorias de Prensa' ),
				'rewrite' => array( 'slug' => 'prensa_categoria' ),
				'hierarchical' => true,
			)
		);
	}

	add_action( 'init', 'crear_columnistas_columna' );
	function crear_columnistas_columna() {
		register_taxonomy('columna_columnista','columna',
			array(
				'label' => __( 'Columnistas de Columna' ),
				'rewrite' => array( 'slug' => 'columna_columnista' ),
				'hierarchical' => true,
			)
		);
	}

	add_action( 'init', 'crear_categorias_entrevista' );
	function crear_categorias_entrevista() {
		register_taxonomy('entrevista_categoria','entrevista',
			array(
				'label' => __( 'Categorias de Entrevista' ),
				'rewrite' => array( 'slug' => 'entrevista_categoria' ),
				'hierarchical' => true,
			)
		);
	}

	add_action( 'init', 'crear_categorias_recomendado' );
	function crear_categorias_recomendado() {
		register_taxonomy('recomendado_categoria','recomendado',
			array(
				'label' => __( 'Categorias de Recomendado' ),
				'rewrite' => array( 'slug' => 'recomendado_categoria' ),
				'hierarchical' => true,
			)
		);
	}

	add_action( 'init', 'crear_categorias_cedle' );
	function crear_categorias_cedle() {
		register_taxonomy('cedle_categoria','equipo_cedle',
			array(
				'label' => __( 'Categorias del equipo cedle' ),
				'rewrite' => array( 'slug' => 'cedle_categoria' ),
				'hierarchical' => true,
			)
		);
	}
	add_action( 'init', 'crear_tipo_evento' );
	function crear_tipo_evento() {
		register_taxonomy('evento_tipo','evento',
			array(
				'label' => __( 'Tipo de evento' ),
				'rewrite' => array( 'slug' => 'evento_tipo' ),
				'hierarchical' => true,
			)
		);
	}

	add_action( 'init', 'normativa_autor' );
	function normativa_autor() {
		register_taxonomy('autor_normativa','normativa',
			array(
				'label' => __( 'Autor' ),
				'rewrite' => array( 'slug' => 'autor_normativa' ),
				'hierarchical' => true,
			)
		);
	}




	if( function_exists('acf_add_options_sub_page') ){
		acf_add_options_sub_page( 'Home' );
		acf_add_options_sub_page( 'Noticia destacada Home' );
		// acf_add_options_sub_page( 'Contacto' );
	}
	function pr($data){
		echo '<pre>';
		print_r($data);
		echo '</pre>';
	}
	function img_taxonomy_publicaciones($id){
		switch ($id) {
			case 6:$img_taxonomy='<img src="'.get_bloginfo("template_url").'/img/icn_cuaderno.png"> ';break;
			case 5:$img_taxonomy='<img src="'.get_bloginfo("template_url").'/img/icn_estudios.png"> ';break;
			case 3:$img_taxonomy='<img src="'.get_bloginfo("template_url").'/img/icn_informe.png"> ';break;
		}
		return $img_taxonomy;
	}
	function img_taxonomy_publicaciones_tipo($id){
		switch ($id) {
			case 25:$img_taxonomy='<img src="'.get_bloginfo("template_url").'/img/icn_publicidad_audio.png"> ';break;
			case 23:$img_taxonomy='<img src="'.get_bloginfo("template_url").'/img/icn_publicidad_pdf.png"> ';break;
			case 26:$img_taxonomy='<img src="'.get_bloginfo("template_url").'/img/icn_publicidad_video.png"> ';break;
			case 24:$img_taxonomy='<img src="'.get_bloginfo("template_url").'/img/icn_documento.png"> ';break;
		}
		return $img_taxonomy;
	}
	function diasMes($mes=0, $ano=0) {
		if ($mes==0) {
			$mes=date('m');
		}
		if ($ano==0) {
			$ano=date('Y');
		}
	   	return date("d",mktime(0,0,0,$mes+1,0,$ano));
	}
	function mes_espanol($id_mes){
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		return $meses[$id_mes-1];
	}
	function cortar_palabras($string, $limit=130, $break=" ", $pad="...") {
		if(strlen($string) <= $limit)
			return $string;
		if(false !== ($breakpoint = strpos($string, $break, $limit))) {
			if($breakpoint < strlen($string)-1) {
			$string = substr($string, 0, $breakpoint);
			}
		}
		$string=sacarUltimaPalabra($string);
		$string=$string.$pad;
		return $string;
	}
	function sacarUltimaPalabra($cadena){
		$Ecadena=explode(' ',$cadena);
		$Ccadena=count($Ecadena);
		$CRcadena=$Ccadena-1;
		$Cletras=strlen($Ecadena[$CRcadena]);
		$Cletras2=strlen($cadena);
		$CTotal=$Cletras2-$Cletras;
		$cadena=substr($cadena,0,$CTotal);
		return trim($cadena);
	}
	function disqus_embed($disqus_shortname) {
    	global $post;
	    wp_enqueue_script('disqus_embed','http://'.$disqus_shortname.'.disqus.com/embed.js');
	    echo '<div id="disqus_thread"></div>
	    <script type="text/javascript">
	        var disqus_shortname = "'.$disqus_shortname.'";
	        var disqus_title = "'.$post->post_title.'";
	        var disqus_url = "'.get_permalink($post->ID).'";
	        var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
	    </script>';
	}
?>