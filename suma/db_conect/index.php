<?php
require("db.php");
$db       = new DB();
// $reclamos = $db->query("UPDATE `reclamos` SET deleted=1 WHERE id <=13"); 
$reclamos = $db->query("SELECT * FROM `landing` where deleted = 0 ");
// print_r($reclamos);
header("Content-type: application/excel");
header("Content-Disposition: attachment; filename=reporte.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Reporte de Reclamos</title>
</head>
<body>
<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Nombre</th>
			<th>Rut</th>
			<th>Cargo</th>
			<th>Instituci&oacute;n</th>
			<th>Correo</th>
			<th>Ciudad</th>
		</tr>
	</thead>
	<tbody>
	<?php
	function acentos($val=''){
		$val=str_replace('á', '&aacute;', $val);
		$val=str_replace('é', '&eacute;', $val);
		$val=str_replace('í', '&iacute;', $val);
		$val=str_replace('ó', '&oacute;', $val);
		$val=str_replace('ú', '&uacute;', $val);
		$val=str_replace('Á', '&Aacute;', $val);
		$val=str_replace('É', '&Eacute;', $val);
		$val=str_replace('Í', '&Iacute;', $val);
		$val=str_replace('Ó', '&Oacute;', $val);
		$val=str_replace('Ú', '&Uacute;', $val);
		$val=str_replace('ñ', '&ntilde;', $val);
		$val=str_replace('Ñ', '&Ntilde;', $val);
		return $val;
	}
	foreach ($reclamos as $key => $value) {



		echo '
		<tr>
			<td>'.$value['id'].'</td>
			<td>'.utf8_decode($value['nombre']).'</td>
			<td>'.$value['rut'].'</td>
			<td>'.utf8_decode($value['cargo']).'</td>
			<td>'.utf8_decode($value['institucion']).'</td>
			<td>'.$value['correo'].'</td>
			<td>'.$value['ciudad'].'</td>
		</tr>';
	}
	?>
	</tbody>
</table>
</body>
</html>