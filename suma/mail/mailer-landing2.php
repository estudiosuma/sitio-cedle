<?php
	function mail_landing($nombre='' ,$rut='' ,$cargo='' ,$institucion='' ,$email='' ,$ciudad='' ){
		require ('class.phpmailer.php');
		require("class.smtp.php");

		//Especificamos los datos y configuración del servidor
		$mail = new PHPMailer;
		$mail->SetFrom('estudiosuma021@gmail.com','Contacto landing');
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';                                     
		$mail->Host = 'smtp.gmail.com';  // Specify main and backup server
		$mail->Port = '587';


                              
		//Nos autenticamos con nuestras credenciales en el servidor de correo Gmail
		$mail->Username = 'estudiosuma021@gmail.com';       
		$mail->Password = 'suma.adwords';
		
		//Agregamos la información que el correo requiere                        
		// $mail->FromName = 'Contacto landing';
		$mail->Subject = 'Nueva Subscripción Landing';

		$mat_mailer='fsaucedo@suma.cl';
		$mail->AddAddress($mat_mailer);
		// $mail->AddReplyTo($mat_email, $mat_nombre . ' ' . $mat_apepri);
		// $mail->AddCC($copia);
		
		$mail->IsHTML(true);// Set email format to HTML
		
		$mail->Body = file_get_contents('../mail/formato_landing.html');
		// $mail->Body = file_get_contents('formato-landing.html');

		$mail->Body = str_replace('{NOMBRE}', $nombre, $mail->Body);
		$mail->Body = str_replace('{RUT}', $rut, $mail->Body);
		$mail->Body = str_replace('{EMAIL}', $email, $mail->Body);
		$mail->Body = str_replace('{INSTITUCION}', $institucion, $mail->Body);
		$mail->Body = str_replace('{CARGO}', $cargo, $mail->Body);
		$mail->Body = str_replace('{CIUDAD}', $ciudad, $mail->Body);
		
		//Enviamos el correo electrónico
		if(!$mail->Send()) {
		   	return '<div class="error"><p>Ha ocurrido un error, inténtenlo más tarde</p></div>'.$mail->ErrorInfo;
		}else{
			return '<div id="content-msj-exito">Su subscripción fuen ingresada con exíto.</div>';
		}
	}