<?php
	function mail_landing($nombre='' ,$correo='' ,$asunto='' ,$mensaje=''){

		$mail_body = file_get_contents('../mail/formato_inscribete.html');
		
		$mail_body = str_replace('{NOMBRE}', $nombre, $mail_body);
		$mail_body = str_replace('{CORREO}', $correo, $mail_body);
		$mail_body = str_replace('{ASUNTO}', $asunto, $mail_body);
		$mail_body = str_replace('{MENSAJE}', $mensaje, $mail_body);
		
		// $headers .= 'Bcc: fsaucedo@estudiosuma.cl' . "\r\n";
		$headers = 'MIME-Version: 1.0'."\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
		$tipo = 'Nueva Subscripción';
		$para  = 'mariana@suma.cl';

		$mail_enviado= mail($para, $tipo, $mail_body, $headers);
		//Enviamos el correo electrónico
		if(!$mail_enviado) {
		   	return '<div id="content-msj-exito">Ha ocurrido un error, inténtenlo más tarde</div>';
		}else{
			return '<div id="content-msj-exito">Gracias. Tu inscripción ha sido realizada con éxito.</div>';
		}
	}