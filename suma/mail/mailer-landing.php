<?php
	function mail_landing($nombre='' ,$rut='' ,$cargo='' ,$institucion='' ,$email='' ,$ciudad='' ){

		$mail_body = file_get_contents('../mail/formato_landing.html');
		
		$mail_body = str_replace('{NOMBRE}', $nombre, $mail_body);
		$mail_body = str_replace('{RUT}', $rut, $mail_body);
		$mail_body = str_replace('{EMAIL}', $email, $mail_body);
		$mail_body = str_replace('{INSTITUCION}', $institucion, $mail_body);
		$mail_body = str_replace('{CARGO}', $cargo, $mail_body);
		$mail_body = str_replace('{CIUDAD}', $ciudad, $mail_body);
		
		// $headers .= 'Bcc: fsaucedo@estudiosuma.cl' . "\r\n";
		$headers = 'MIME-Version: 1.0'."\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
		$tipo = 'Nueva Subscripción Landing';
		$para  = 'fsaucedo@suma.cl';

		$mail_enviado= mail($para, $tipo, $mail_body, $headers);
		//Enviamos el correo electrónico
		if(!$mail_enviado) {
		   	return 'error';
		}else{
			return 'bien';
		}
	}