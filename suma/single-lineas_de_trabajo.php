<?php get_header(); 
$id = get_the_ID(); 
if(get_field('imagen_de_fondo')){ ?>
    <style type="text/css">
    	.section_linea {
    	    background: #efefef url("<?php echo get_field('imagen_de_fondo'); ?>") no-repeat scroll -1px top;
    	}
    </style>
<?php } ?>
	<!-- CONTENIDO PRINCIPAL-->
    <div class="col-md-9">
        <!-- Contenido seccion -->
        <div class="section section_linea row">
            <div class="col-xs-6 nav-left">
                <a href="/lineas-de-trabajo/"><i class="fa fa-arrow-left"></i> Lineas de Trabajo</a>
            </div>
            <div class="col-xs-6 nav-right">
            <?php $next_post = get_next_post(); ?>
            <?php if ( is_a( $next_post, 'WP_Post' ) ) {  ?>
                <a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo get_the_title( $next_post->ID ); ?> <i class="fa fa-arrow-right"></i></a>
            <?php } ?>
            </div>
            <div class="col-md-12">
                <div class="text-center top">
                <img src="<?php bloginfo('template_url');?>/img/icn_linea.png">
                <h1><?php the_title(); ?></h1>
                </div>
                <div class="padre_principal">
                    <div class="row principal  ">
                        <div class="col-md-6">
                            <?php echo get_field('texto_columna_izquierda'); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo get_field('texto_columna_derecha'); ?>
                        </div>
                    </div>
                    <?php
                    query_posts(array('showposts' => 1, 'post_type' => 'equipo_trabajo', 'order'=> 'ASC','meta_query'=> array(
                                array(
                                    'key' => 'linea_de_trabajo',
                                    'value' =>$id,
                                    'compare' => 'LIKE'
                                    ),
                                ), 'orderby' => 'order'));
                    if ( have_posts() ): ?>
                    <div class="row secundario  ">
                        <div class="col-md-12">
                            <h3>Equipo de Trabajo</h3>
                        </div>
                        <?php while (have_posts()) :the_post(); 
                                $x=0;
                                if( have_rows('equipo_de_trabajo') ):
                                    while( have_rows('equipo_de_trabajo') ): the_row();
                                        if ($x==3) {
                                            $x=0;
                                            echo '<div class="clearfix"></div>';
                                        }
                                        echo '  <div class="col-md-4">
                                                    <h4>'.get_sub_field('nombre').'</h4>
                                                    <span>'.get_sub_field('cargo').'</span>
                                                    <p>'.get_sub_field('descripcion').'</p>
                                                </div>';
                                        $x++;
                                    endwhile;
                                endif;
                            endwhile; ?>
                    </div>
                    <?php endif; ?>
                </div>  
            </div>
        </div> 
<?php get_footer(); ?>
