<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage SUMA
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<?php if(get_field('imagen_de_fondo')){ ?>
    <meta property="og:image" content="<?php echo get_field('imagen_de_fondo'); ?>" />
<?php }else{ ?>
    <meta property="og:image" content="<?php bloginfo('template_url');?>/img/logo_facebook.png" />
<?php } ?>
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
    <!-- Bootstrap Core CSS -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,700,600italic,700italic,400italic,300italic' rel='stylesheet' type='text/css'>
    <link href="<?php bloginfo('template_url');?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php bloginfo('template_url');?>/css/bootstrap-select.css" rel="stylesheet">
    <link href="<?php bloginfo('template_url');?>/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/jquery.mCustomScrollbar.css">
    <link href="<?php bloginfo('template_url');?>/css/main.css" rel="stylesheet">
    <link href="<?php bloginfo('template_url');?>/css/suma.css" rel="stylesheet"> 
    <link href="<?php bloginfo('template_url');?>/css/responsive.css" rel="stylesheet"> 

    <!-- Custom CSS 
    <link href="<?php bloginfo('template_url');?>/css/simple-sidebar.css" rel="stylesheet">-->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body >
	<div class="container">
        <div class="row"> 
            <!-- MENU PRINCIPAL -->
            <div class="col-md-3" id="padre-side">
                <!-- Sidebar -->
                <div class="menu-lateral" id="sidesbar-wrapper">
                    <a href="/index/" id="logo">Centro de Desarrollo del Liderazgo Educativo</a> 
                    <a href="#" id="menu-toggle"> 
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span> 
                      </a>
                    <ul class="sidebar-navs"> 
                        <li>
                            <a href="/index/">Inicio</a>
                        </li>
                        <li>
                            <a href="/nosotros/">Nosotros</a>
                        </li>
                        <li>
                            <a href="/lineas-de-trabajo/">Líneas de trabajo</a>
                        </li>
                        <li>
                            <a href="/actualidad/">Actualidad</a>
                        </li>
                        <li>
                            <a href="/publicacion/">Publicaciones</a>
                        </li>
                        <li>
                            <a href="/noticias-y-prensa/">Noticias & Prensa</a>
                        </li>
                        <li>
                            <a href="/agenda/">Agenda</a>
                        </li>
                        <!-- <li>
                            <a href="#">Galerías</a>
                        </li> -->
                        <li>
                            <a href="/contacto/">Contacto</a>
                        </li>
                        <!-- <li class="lines">
                            <a href="#">Plataforma</a>
                        </li> -->
                    </ul>
                    <div class="box-suscribete">
                        <p>Suscríbete a nuestro <strong>Newsletter</strong></p>
                        <span class="error">Ingresa Tu Correo</span>
                        <input id="correo-newsletter" placeholder="Ingresa tu correo aquí" name="correo" type="text" class="text">
                        <a id="inscribir-newsletter" href="#" class="agregar"><i class="fa fa-plus"></i></a>
                    </div>
                    <div class="box-buscador">
                        <div class="box">
                            <a href="#" class="btn-buscar btn-search-home"><i class="fa fa-search"></i></a>
                            <form  id="form_search_home" action="<?php echo home_url(); ?>"   method="GET">
                            <input name="s" type="text" class="text-buscar"> 
                            <div class="option">
                                <div>
                                    <label>
                                        <input name="filtro_formulario[]" value="persona" type="checkbox">
                                        <i class="fa fa-user"></i>
                                        Personas
                                    </label>
                                    <label>
                                        <input name="filtro_formulario[]" value="publicacion" type="checkbox">
                                        <i class="fa fa-download"></i>
                                        Publicaciones
                                    </label>
                                    <label>
                                        <input name="filtro_formulario[]" value="noticia_prensa" type="checkbox">
                                        <i class="fa fa-file-text-o"></i>
                                        Noticias & prensa
                                    </label>
                                </div>
                            </div>
                            </form>
                        </div> 
                    </div>
                    <div class="box-redes">
                        <a href="https://www.facebook.com/cedlechile"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/CedleChile"><i class="fa fa-twitter"></i></a>
                    </div>
                    
                </div>
                <!-- /#sidebar-wrapper -->
            </div>