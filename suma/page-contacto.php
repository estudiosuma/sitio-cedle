<?php
/*Template Name: Contacto */
get_header(); ?>

<style>
    <?php if(get_field('imagen_fondo')){?>
    .section_contacto {
        background: #efefef url('<?php echo get_field('imagen_fondo'); ?>') -1px top no-repeat;
    }
    <?php }else{ ?>
        .section_contacto {
            background: #efefef -1px top no-repeat;
        }
    <?php }?>
</style>
<!-- CONTENIDO PRINCIPAL-->
            <div class="col-md-9">

                <!-- Contenido seccion -->
                <div class="section section_contacto row">
                    <div class="col-xs-6 nav-left">
                        <a href="/index/"><i class="fa fa-arrow-left"></i> Home</a>
                    </div>
                    <div class="col-xs-6 nav-right">
                         
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="text-center top">
                            <img src="<?php echo get_field('icono'); ?>">
                            <h1>Contáctanos</h1> 
                        </div>

                        <div class="row row-cont">
                            <div class="col-md-5 left">
                                <h2><?php echo get_field('titulo'); ?></h2>
                                <p><?php echo get_field('bajada'); ?></p>
                            </div>
                            <div class="col-md-7 right">
                                <form id="id-formulario-contacto">
                                    <div>
                                        <label>Nombre</label>
                                        <input type="text" id="inputnombre" name="nombre" class="text">
                                        <span class="error">Ingresa Tu Nombre</span>
                                    </div> 
                                    <div>
                                        <label>Correo</label>
                                        <input type="text" id="inputcorreo" name="nombre" class="text">
                                        <span class="error">Ingresa Tu Correo</span>
                                    </div>
                                    <div class="asunto">
                                        <label>Asunto</label>
                                        <select id="selectcargo" name="cargo" class="selectpicker" >
                                            <option value="Director/a" >Director/a</option>
                                            <option value="Docente" >Docente</option>
                                            <option value="Jefe/a de UTP" >Jefe/a de UTP</option>
                                            <option value="Otro" >Otro</option>
                                            <option value="Inspector/a  " >Inspector/a</option>
                                        </select>
                                        <input type="text" id="inputasunto" name="asunto" class="text">
                                        <span class="error">Ingresa Tu Asunto</span>
                                    </div>
                                    <div>
                                        <label>Mensaje</label>
                                        <textarea name="mensaje" id="inputmensaje"></textarea>
                                        <span class="error">Ingresa Tu Mensaje</span>
                                    </div>
                                    <a href="#" id="btn-contacto" class="btn btn-primary">Enviar</a>
                                </form>
                            </div>
                        </div>

                        <!-- SECCION 7 -->
                        <div class="section-7 " >
                            <div class="row">  
                                <div class="col-md-5 left ">
                                    <div class="contenido_sucursal">
                                        <?php
                                    query_posts(array('showposts' => 1, 'post_type' => 'sucursal', 'p'=>'57', 'order'=> 'ASC', 'orderby' => 'order'));
                                    if ( have_posts() ):
                                        while (have_posts()) :the_post(); 
                                            echo '  <img src="'.get_field('imagen_home').'">
                                                    <div class="txt">
                                                        <h3>'.get_the_title().'</h3>
                                                        <p>'.get_field('bajada').'</p>';
                                if (get_field('telefono')) :
                                echo '      <div>
                                                <strong>Teléfono</strong>
                                                <span>'.get_field('telefono').'</span>
                                            </div>';
                                endif;
                                if (get_field('direccion')) :
                                echo '      <div>
                                                <strong>Dirección</strong>
                                                <span>'.get_field('direccion').'</span>
                                            </div>';
                                endif;
                                if (get_field('horario')) :
                                echo '      <div>
                                                <strong>Horario de atención</strong>
                                                <span>'.get_field('horario').'</span>
                                            </div>';
                                endif;

                                echo '      </div>';
                                        endwhile;
                                    endif;
                                ?>
                                    </div>
                                    
                                </div>
                                <div class="col-md-7 mapa hidden-xs hidden-sm">
                                     
                                     <a href="#" class="btn_mapa mapa_california"> </a>
                                     <a href="#" class="btn_mapa mapa_santiago active"> </a>
                                     <a href="#" class="btn_mapa mapa_talca"> </a>
                                     <a href="#" class="btn_mapa mapa_temuco"> </a> 
                                </div>
                            </div>
                        </div>
                        <!-- FIN SECCION 7 -->
                         
                    </div>
                </div>
                <!-- SECCION SUCURSALES -->
                <div class="section-hiden" style="display: none;">
                    <div id="div-contenedor-mapa_california">
                    <?php
                        query_posts(array('showposts' => 1, 'post_type' => 'sucursal', 'p'=>'55', 'order'=> 'ASC', 'orderby' => 'order'));
                        if ( have_posts() ):
                            while (have_posts()) :the_post(); 
                                echo '  <img src="'.get_field('imagen_home').'">
                                        <div class="txt">
                                            <h3>'.get_the_title().'</h3>
                                            <p>'.get_field('bajada').'</p>';
                                if (get_field('telefono')) :
                                echo '      <div>
                                                <strong>Teléfono</strong>
                                                <span>'.get_field('telefono').'</span>
                                            </div>';
                                endif;
                                if (get_field('direccion')) :
                                echo '      <div>
                                                <strong>Dirección</strong>
                                                <span>'.get_field('direccion').'</span>
                                            </div>';
                                endif;
                                if (get_field('horario')) :
                                echo '      <div>
                                                <strong>Horario de atención</strong>
                                                <span>'.get_field('horario').'</span>
                                            </div>';
                                endif;

                                echo '      </div>';
                            endwhile;
                        endif;
                    ?>
                    </div>
                    <div id="div-contenedor-mapa_santiago">
                        <?php
                        query_posts(array('showposts' => 1, 'post_type' => 'sucursal', 'p'=>'57', 'order'=> 'ASC', 'orderby' => 'order'));
                        if ( have_posts() ):
                            while (have_posts()) :the_post(); 
                                echo '  <img src="'.get_field('imagen_home').'">
                                        <div class="txt">
                                            <h3>'.get_the_title().'</h3>
                                            <p>'.get_field('bajada').'</p>';
                                if (get_field('telefono')) :
                                echo '      <div>
                                                <strong>Teléfono</strong>
                                                <span>'.get_field('telefono').'</span>
                                            </div>';
                                endif;
                                if (get_field('direccion')) :
                                echo '      <div>
                                                <strong>Dirección</strong>
                                                <span>'.get_field('direccion').'</span>
                                            </div>';
                                endif;
                                if (get_field('horario')) :
                                echo '      <div>
                                                <strong>Horario de atención</strong>
                                                <span>'.get_field('horario').'</span>
                                            </div>';
                                endif;

                                echo '      </div>';
                            endwhile;
                        endif;
                    ?>
                    </div>
                    <div id="div-contenedor-mapa_talca">
                        <?php
                        query_posts(array('showposts' => 1, 'post_type' => 'sucursal', 'p'=>'58', 'order'=> 'ASC', 'orderby' => 'order'));
                        if ( have_posts() ):
                            while (have_posts()) :the_post(); 
                                echo '  <img src="'.get_field('imagen_home').'">
                                        <div class="txt">
                                            <h3>'.get_the_title().'</h3>
                                            <p>'.get_field('bajada').'</p>';
                                if (get_field('telefono')) :
                                echo '      <div>
                                                <strong>Teléfono</strong>
                                                <span>'.get_field('telefono').'</span>
                                            </div>';
                                endif;
                                if (get_field('direccion')) :
                                echo '      <div>
                                                <strong>Dirección</strong>
                                                <span>'.get_field('direccion').'</span>
                                            </div>';
                                endif;
                                if (get_field('horario')) :
                                echo '      <div>
                                                <strong>Horario de atención</strong>
                                                <span>'.get_field('horario').'</span>
                                            </div>';
                                endif;

                                echo '      </div>';
                            endwhile;
                        endif;
                    ?>
                    </div>
                    <div id="div-contenedor-mapa_temuco">
                        <?php
                        query_posts(array('showposts' => 1, 'post_type' => 'sucursal', 'p'=>'59', 'order'=> 'ASC', 'orderby' => 'order'));
                        if ( have_posts() ):
                            while (have_posts()) :the_post(); 
                                echo '  <img src="'.get_field('imagen_home').'">
                                        <div class="txt">
                                            <h3>'.get_the_title().'</h3>
                                            <p>'.get_field('bajada').'</p>';
                                if (get_field('telefono')) :
                                echo '      <div>
                                                <strong>Teléfono</strong>
                                                <span>'.get_field('telefono').'</span>
                                            </div>';
                                endif;
                                if (get_field('direccion')) :
                                echo '      <div>
                                                <strong>Dirección</strong>
                                                <span>'.get_field('direccion').'</span>
                                            </div>';
                                endif;
                                if (get_field('horario')) :
                                echo '      <div>
                                                <strong>Horario de atención</strong>
                                                <span>'.get_field('horario').'</span>
                                            </div>';
                                endif;

                                echo '      </div>';
                            endwhile;
                        endif;
                    ?>
                    </div>
                </div>  
                <!-- FIN SECCION SUCURSALES -->
<?php get_footer(); ?>
