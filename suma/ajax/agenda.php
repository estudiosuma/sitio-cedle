<?php  
require_once("../../../../wp-load.php");
// pr($_POST);
    $dias_mes = diasMes($_POST['mes_agenda'],$_POST['ano_agenda']);
    // echo date('dmY').'-'.$dias_mes.date('mY');
    $inicio_de_mes= $_POST['ano_agenda'].$_POST['mes_agenda'].'01';
    query_posts(array('showposts' => 100, 'post_type' => 'evento', 'order'=> 'ASC', 'orderby' => 'order','meta_query' => array(
		            'relation' => 'AND',
		            array(
		                'key' => 'fecha_del_evento',
		                'value' => $inicio_de_mes,
		                'compare' =>'>='
		                 ),
		            array(
		                'key' => 'fecha_del_evento',
		                'value' => $_POST['ano_agenda'].$_POST['mes_agenda'].$dias_mes,
		                'compare' =>'<='
		                 )
		           )));
    if ( have_posts() ):
    	$x=0;
        while (have_posts()) :the_post(); 
        	$posisicion[$x]=$post->ID;
            $taxonomy3="evento_tipo";
            $temas='';
            $terms3 = wp_get_post_terms( $post->ID, $taxonomy3 );
            foreach ($terms3 as $key => $value) {
                if ($key>0) {
                    $temas.=', ';
                }
                $temas.= $value->name;
            }
            $date = get_field('fecha_del_evento');
            $url=get_permalink( $post->ID );
            $fecha_del_evento=date('d',strtotime($date));
            $html[$fecha_del_evento]['id']=$post->ID;
            $html[$fecha_del_evento]['img']='  <img src="'.get_field('imagen_home').'">';
            $html[$fecha_del_evento]['small']='<small>'.date('d',strtotime($date)).' '.mes_espanol(date('n',strtotime($date))).', '.date('Y',strtotime($date)).'</small>';
            $html[$fecha_del_evento]['span']='<span>'.$temas.'</span>';
            $html[$fecha_del_evento]['h3']='<h3>'.get_the_title().'</h3>';
            $html[$fecha_del_evento]['p']='<p>'.get_field('descripcion_del_evento').'</p>';
            if ($date>= date('Ymd')) {
                if (get_field('activar_boton')) {
                    if (get_field('tipo_boton')=='modal') {
                        $html[$fecha_del_evento]['a']='<a href="#"  class="btn btn-primary btn-inscribirme" data-nombre_evento="'.get_the_title().'>Inscribirme</a>';
                    }else{
                        $html[$fecha_del_evento]['a']='<a href="'.get_field('url_boton').'"  target="_blank" class="btn btn-primary " data-nombre_evento="'.get_the_title().'"">'.get_field('texto_de_boton').'</a>';
                    }
                }
            }
            $x++;            
        endwhile;
    endif; ?>
    <div class="rows">
        <div class="col-md-5 calendario">
            <div class="row">
                <?php 
                $x=1;
                $z=0;
                $y=0;
                $w=0;
                for ($i=1; $i <= $dias_mes; $i++) { 
                    switch ($x) {
                        case '1':$numero='uno';$x++;break;
                        case '2':$numero='dos';$x++;break;
                        case '3':$numero='tres';$x=1;break;
                    }
                    if ($html[$i]['id']) {
                        echo '<a class="col-md-2 activo '.$numero.'"  data-evento="'.$html[$i]['id'].'" >'.$i.'</a>';
                        if ($w==0 && $z!=0) {
                        	$w=$i;
                        }
                        if ($z==0) {
                            $z=$i;
                        }
                    }else{
                        echo '<div class="col-md-2 '.$numero.'"  >'.$i.'</div>';
                    }
                    $y=$y+2;
                }
                $tamano_barra_evento=$y%12;
                $tamano_barra_evento=12-$tamano_barra_evento;
                // echo $tamano_barra_evento;
                if ($tamano_barra_evento<=4) {
                    $tamano_barra_evento=12;
                }
                ?>
                <div class="col-md-<?php echo $tamano_barra_evento;?> btn-selecciona" >Selecciona un evento para visualizar  ></div> 
            </div>
        </div>
        <div class="col-md-7 item-evento">
            <!-- aca cargar nuevo evento -->
            	<?php if($html[$z]['id']){ ?>
                	<?php echo $html[$z]['img']; ?>
                    <div class="txt scroll dark" data-mcs-theme="minimal-dark">
                        <?php echo $html[$z]['small']; ?>
                        <?php echo $html[$z]['span']; ?>
                        <?php echo $html[$z]['h3']; ?>
                        <?php echo $html[$z]['p']; ?>
                        <?php echo $html[$z]['a']; ?>
                        <div class="col-md-12 redes-evento-<?php echo $html[$z]['id'];?>">
                        </div>
                        <div class="nav">
                        	<?php if($html[$w]['id']){ ?>
                            <!-- <a href="#" class="evento-anterior"><i class="fa fa-arrow-left"></i></a>  -->
                            <?php } ?>
                            <?php if($html[$w]['id']){ ?>
                            <a href="#" class="evento-posterior" data-evento="<?php echo $html[$w]['id']; ?>"><i class="fa fa-arrow-right"></i></a> 
                            <?php } ?>
                        </div>
                    </div>
                <?php }else{ ?>
                	<style type="text/css">
                	.section_agenda .sections-8 .item-evento {
					    background: #3c3c3c none repeat scroll 0 0;
					    height: 480px;
					    margin-left: -1px;
					    padding: 0;
					}
					.col-md-7.item-evento > p {
					    color: #fff;
					    font-size: 16px;
					    margin-top: 200px;
					    text-align: center;
					}
                	</style>
                	<p>No se encuentran eventos para la fecha seleccionada. Elige el mes siguiente para ver nuestros próximos eventos.</p>
                <?php } ?>
            <!--  fin  -->  
        </div>
    </div>
    <!-- SECCION EVENTOS -->
    <div class="section-hiden">
        <?php
        	$x=0;
            foreach ($html as $key => $value) {
                echo '<div id="evento-'.$value['id'].'" >';
                    echo $html[$key]['img'];
					echo '<div class="txt  dark" >';
					echo $html[$key]['small']; 
					echo $html[$key]['span'];
					echo $html[$key]['h3']; 
					echo $html[$key]['p'];
					echo $html[$key]['a'];
                    echo '<div class="col-md-12"> ';
                    echo '</div>';
					echo '<div class="nav">';
					if($html[($key-1)]['id']){ 
					echo '<a  class="evento-anterior" data-evento="'.$html[($key-1)]['id'].'"><i class="fa fa-arrow-left"></i></a>';
					}
					 if($html[($key+1)]['id']){ 
					echo '<a  class="evento-posterior" data-evento="'.$html[($key+1)]['id'].'"><i class="fa fa-arrow-right"></i></a> ';
					 } 
					echo '</div>';
					echo "<script type='text/javascript'>
						    $(document).ready(function() {
								  	//modal incribirse
									$( '.btn-inscribirme' ).click(function() {
                                        $('#nombre-evento').html($(this).data('nombre_evento'));
										$('#modal-inscripcion').modal('show'); 
									  	// return false
									});
							});
						    </script>";
                echo '</div>
                </div>';
            }
        ?>
    </div>
    <script type="text/javascript">
    $(document).ready(function() { 
    	$( ".calendario .activo" ).click(function() {
			/* contenido simulado */
			var evento = $('#evento-'+$(this).data('evento')).html();
			console.log($(this).data('evento'));
			/* fin contenido simulado */
		  	$('.item-evento').fadeOut( "slow", function() {
			    $('.item-evento').html(evento);
                $('.item-evento .txt').addClass("scroll");
                $('.item-evento .txt').data( "mcs-theme",'minimal-dark' );
			  	$('.item-evento').fadeIn();
                $(".scroll").mCustomScrollbar({
                    scrollButtons:{enable:true},
                    theme:"light-thick",
                    scrollInertia:100,
                    scrollbarPosition:"inside"
                });

		  	});
            var evento_id = $(this).data('evento');
            setTimeout(function(){
            $('.item-evento .txt .col-md-12').addClass("redes-evento-"+evento_id );
            $.ajax({
                   url: "/wp-content/themes/suma/ajax/redes_evento.php",  
                   type: "POST",
                   data: {
                            id:evento_id 
                    },
                   async: true,
                   cache: false,
                   success:function(response,textStatus,xhr,data,callback,result){
                        console.log($('.item-evento .txt .redes-evento-'+evento_id ).length);
                       $('.item-evento .txt .redes-evento-'+evento_id ).html(response);
                   },
                  error: function(){
                }
            });
            },1000);
		  	return false;
	  	});
        $(".scroll").mCustomScrollbar({
            scrollButtons:{enable:true},
            theme:"light-thick",
            scrollInertia:100,
            scrollbarPosition:"inside"
        });
        $('.section-hiden').hide();
        $.ajax({
               url: "/wp-content/themes/suma/ajax/redes_evento.php",  
               type: "POST",
               data: {
                        id:'<?php echo $html[$z]['id'];?>'
                },
               async: true,
               cache: false,
               success:function(response,textStatus,xhr,data,callback,result){
                   $('.redes-evento-<?php echo $html[$z]['id'];?>').html(response);
               },
              error: function(){
            }
        });
// jQuery('body').trigger( 'post-load');
	});
    </script>