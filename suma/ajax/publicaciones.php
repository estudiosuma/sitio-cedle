<?php
require_once("../../../../wp-load.php"); ?>

<?php 
$query=array('showposts' => 1000, 'post_type' => 'publicaciones','order'=> 'DESC', 'orderby' => 'order');

if ($_POST['select_publicacion_ano'] && $_POST['select_publicacion_autor']) {
	$query['meta_query'] = array(
								'relation' => 'AND',
									array(
										'key'     => 'anos_de_publicacion',
										'value'   => $_POST['select_publicacion_ano'],
										// 'type'    => 'numeric',
										// 'compare' => 'LIKE',
									),
									array(
										'key'     => 'autor',
										'value'   => '"'.$_POST['select_publicacion_autor'].'"',
										// 'type'    => 'numeric',
										'compare' => 'LIKE',
									)
							);
}else if ($_POST['select_publicacion_ano'] && !$_POST['select_publicacion_autor']) {
	$query['meta_query'] = array(
								'relation' => 'AND',
									array(
										'key'     => 'anos_de_publicacion',
										'value'   => $_POST['select_publicacion_ano'],
										// 'type'    => 'numeric',
										// 'compare' => 'LIKE',
									)
							);
}else if (!$_POST['select_publicacion_ano'] && $_POST['select_publicacion_autor']) {
	$query['meta_query'] = array(
								'relation' => 'AND',
									array(
										'key'     => 'autor',
										'value'   => '"'.$_POST['select_publicacion_autor'].'"',
										// 'type'    => 'numeric',
										'compare' => 'LIKE',
									)
							);
}

if ($_POST['select_publicacion_tema']) {
		$query['tax_query'] = array(
			array(
				'taxonomy' => 'publicaciones_tema', //or tag or custom taxonomy
				'field' => 'id',
				'terms' => array($_POST['select_publicacion_tema'])
			)
		);
	}

// pr($query);
// exit();
query_posts($query);
// count(have_posts());
if ( have_posts() ):
    $x=0;
    $taxonomy="publicaciones_categoria";
    $taxonomy2="publicaciones_tipo";
    $taxonomy3="publicaciones_tema";
    while (have_posts()) :the_post();
        $id_taxonomy_select=0;
        $terms = wp_get_post_terms( $post->ID, $taxonomy );
        foreach ($terms as $value) {
            if ($value->term_id!=4) {
                $id_taxonomy_select = $value;
            }
        }
        $terms2 = wp_get_post_terms( $post->ID, $taxonomy2 );
        $terms3 = wp_get_post_terms( $post->ID, $taxonomy3 );
        $temas='';
        foreach ($terms3 as $key => $value) {
        	if ($key>0) {
        		$temas.=', ';
        	}
        	$temas.= $value->name;
        }
        $id_post_no_repetir = $post->ID;
        $post_object = get_field('autor');
        $autores='';
        if($post_object){
            foreach ($post_object as $key => $value) {
                if ($key>0) {
                    $autores.=', ';
                }
                $autores.= $value->post_title;
            }
        }
        ?>
    <div class="col-md-4 items" data-groups='["<?php echo $id_taxonomy_select->name; ?>"]'>
        <div class="box-publicaciones">
            <?php echo img_taxonomy_publicaciones($id_taxonomy_select->term_id); ?>
            <!-- <h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>  -->
            <h3><?php echo cortar_palabras(get_the_title()); ?></h3>
            <div class="detalle">
                <p><strong>Fecha:</strong> <?php echo get_field('anos_de_publicacion'); ?></p>
                <p><strong>Autor:</strong> <?php echo cortar_palabras($autores,54);?></p>
                <p><strong>Tema: </strong><?php echo $temas; ?></p>
                <div class="descarga">
                    <?php echo img_taxonomy_publicaciones_tipo($terms2[0]->term_id); ?>
                    <a href="<?php echo get_the_permalink(); ?>">Ver</a> |
                    <a href="<?php echo get_field('archivo'); ?>">Descargar</a>
                </div>
            </div>
        </div>
    </div> 
    <?php
        
    endwhile;
endif;

?>

