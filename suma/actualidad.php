<?php
/*Template Name: Actualidad */
get_header(); ?>
<!-- CONTENIDO PRINCIPAL-->
            <div class="col-md-9">

                <!-- Contenido seccion -->
                <div class="section section_actualidad  ">
                    <div class="col-xs-6 nav-left">
                        <a href="/index/"><i class="fa fa-arrow-left"></i> Home</a>
                    </div>
                    <div class="col-xs-6 nav-right">
                        <a href="/publicacion/">Publicaciones <i class="fa fa-arrow-right"></i></a>
                    </div>
                    <div class="col-md-12">
                            <div class="text-center top">
                                <img src="<?php bloginfo('template_url');?>/img/icn_noticias.png">
                                <h1>ACTUALIDAD</h1>
                            </div> 

                          <!-- SECCION 3 -->
                          <nav class="filter-options row">
                                <!-- <div class="col-xs-3">
                                    <a href="#" data-group="entrevistas" class=" active">Entrevistas</a>
                                </div> -->
                                <div class="col-xs-3">
                                    <a href="#" data-group="columnas"  class=" active">Columnas</a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="#" data-group="recomendados"  >Recomendados</a>
                                </div> 
                                <div class="col-xs-3">
                                    <a href="#" data-group="normativa"  >Normativa</a>
                                </div>
                            </nav>

                            <div class="section-3">
                                <div class="row" >
                                     <div class="clearfix"></div>
                                    <div  >
                                        <div class="col-md-12 "  >
                                            <!-- <div class="box box-entrevista box-destacado">
                                                <img src="<?php bloginfo('template_url');?>/img/cnt_actualidad_destacada.jpg">
                                                <span>Entrevista a : <strong>José weinstein</strong></span>
                                                <h3>“Título o frase extraída de la entrevista, como  destacado”</h3>
                                                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                                                 <a href="#" class="ver-mas"><i class="fa fa-arrow-right"></i></a>
                                            </div> -->
                                        </div> 

                                        <div class="col-md-12 test" id="grid">

                                            <?php
                                                    query_posts(array('showposts' => 100, 'post_type' => array('recomendado','columna','normativa'), 'order'=> 'DESC', 'orderby' => 'order'));
                                                    if ( have_posts() ):
                                                        $x=0;
                                                        $i=0;
                                                        
                                                        while (have_posts()) :the_post(); 
                                                        $data_box_actualidad='';
                                                        $img_actualidad='';
                                                        $span_actualidad='';
                                                        $p_actualidad='';
                                                        $a_actualidad='';
                                                        $taxonomy="columna_columnista";
                                                        $taxonomy2="autor_normativa";
                                                            $x++;
                                                            switch ($post->post_type) {
                                                                case 'columna':$a_actualidad='<a href="'.get_permalink().'" class="ver-mas"><i class="fa fa-arrow-right"></i></a>'; $p_actualidad=""; $data_box_actualidad="columnas"; $class_box_actualidad='box-columna';$terms = wp_get_post_terms( $post->ID, $taxonomy );if (get_field('foto_de_columnista',$taxonomy.'_'.$terms[0]->term_id)) { $img_actualidad='<a href="'.get_permalink().'"><img src="'.get_field('foto_de_columnista',$taxonomy.'_'.$terms[0]->term_id).'"></a>';}else{$img_actualidad='';};$span_actualidad='<span>Columna de: <strong>'.$terms[0]->name.'</strong></span>'; break;
                                                                case 'recomendado':$a_actualidad='<a href="'.get_permalink().'" class="ver-mas"><i class="fa fa-arrow-right"></i></a>'; $p_actualidad='<p>Fuente: <a href="'.get_field('url_fuente').'">'.get_field('fuente').'</a></p>'; $data_box_actualidad="recomendados"; $class_box_actualidad='box-entrevista recomendados';if (get_field('imagen_home')) {$img_actualidad='<a href="'.get_permalink().'"><img src="'.get_field('imagen_home').'"></a>';};$span_actualidad='<span>CEDLE Recomienda</span>'; break;
                                                                case 'entrevista':$a_actualidad='<a href="'.get_permalink().'" class="ver-mas"><i class="fa fa-arrow-right"></i></a>'; $p_actualidad=""; $data_box_actualidad="entrevistas"; $class_box_actualidad='box-entrevista';if (get_field('imagen_home')) {$img_actualidad='<a href="'.get_permalink().'"><img src="'.get_field('imagen_home').'"></a>';};$span_actualidad='<span>Entrevista a : <strong>'.get_field('nombre_entrevistado').'</strong></span>'; break;
                                                                case 'infografia':$a_actualidad=''; $p_actualidad=""; $data_box_actualidad="infografias"; $class_box_actualidad='box-infografia';$img_actualidad='<a href="'.get_permalink().'"><img src="'.get_bloginfo('template_url').'/img/icn_infografia.png"></a>';$span_actualidad='<span>Infografia</span>'; break;
                                                                case 'normativa':$a_actualidad='<a href="'.get_permalink().'" class="ver-mas"><i class="fa fa-arrow-right"></i></a>'; $p_actualidad=""; $data_box_actualidad="normativa"; $class_box_actualidad='box-columna';$terms = wp_get_post_terms( $post->ID, $taxonomy2 );if (get_field('foto_de_autor',$taxonomy2.'_'.$terms[0]->term_id)) { $img_actualidad='<a href="'.get_permalink().'"><img src="'.get_field('foto_de_autor',$taxonomy2.'_'.$terms[0]->term_id).'"></a>';}else{$img_actualidad='';};$span_actualidad='<span>Normativa de: <strong>'.$terms[0]->name.'</strong></span>'; break;
                                                                
                                                            }?>
                                                            <div class="col-md-4 items" data-groups='["<?php echo $data_box_actualidad; ?>"]'>
                                                            <?php
                                                            echo '
                                                                <div class="box '.$class_box_actualidad.'">';
                                                            echo '  
                                                                    '.$img_actualidad.'
                                                                    '.$span_actualidad.'
                                                                    <h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>
                                                                    '.$p_actualidad.'
                                                                    '.$a_actualidad.'
                                                                </div>';
                                                            echo '</div>';
                                                        endwhile;
                                                        
                                                    endif;
                                                    ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="box-cargar row">
                                    <a href="#" class="btn btn-primary">Cargar Más</a> 
                                </div> -->
                            </div>
                            <!-- FIN SECCION 3 -->
                        </div>
                    </div>
<?php get_footer(); ?>
