<?php
/*Template Name: Lineas de Trabajo*/
get_header(); ?>
<div class="col-md-9">

                <!-- Contenido seccion -->
                <div class="section section_linea row">
                    <div class="col-xs-6 nav-left">
                        <a href="/index/"><i class="fa fa-arrow-left"></i> Home</a>
                    </div>
                    <div class="col-xs-6 nav-right">
                        <a href="/actualidad/">Actualidad <i class="fa fa-arrow-right"></i></a>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center top">
                        <img src="<?php bloginfo('template_url');?>/img/icn_linea.png">
                        <h1><?php the_title(); ?></h1>
                        </div>

                        <div class="row linea-tiempos ">
                            <?php
                                query_posts(array('showposts' => 10, 'post_type' => 'lineas_de_trabajo', 'order'=> 'ASC', 'orderby' => 'order'));
                                if ( have_posts() ):
                                    $x=0;
                                    $i=0;
                                    $class_first= 'first';
                                    while (have_posts()) :the_post(); 
                                        $x++;
                                        echo '
                                                <div class="col-md-6 item '.$class_first.'">
                                                    <div class="subitem item-'.$x.'">
                                                        <h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>
                                                        <p>'.get_field('texto_resumido').'</p>
                                                        <a href="'.get_the_permalink().'" class="ver-mas"><i class="fa fa-arrow-right"></i></a>
                                                    </div> 
                                                </div>';
                                        if ($x==2 && $i==0) {
                                            $x=0;
                                            $i=1;
                                            $class_first= '';
                                        }elseif ($x==2) {
                                            $x=0;
                                        }

                                    endwhile;
                                endif;

                         ?>
                        </div>
                    </div>
                </div> 
<?php get_footer(); ?>
