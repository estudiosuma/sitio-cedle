<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage SUMA
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
    <!-- Bootstrap Core CSS -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,700,600italic,700italic,400italic,300italic' rel='stylesheet' type='text/css'>
    <link href="<?php bloginfo('template_url');?>/landing/css/bootstrap.css" rel="stylesheet">
    <link href="<?php bloginfo('template_url');?>/landing/css/font-awesome.css" rel="stylesheet"> 
    <link href="<?php bloginfo('template_url');?>/landing/css/landing.css" rel="stylesheet">   
    <link href="<?php bloginfo('template_url');?>/landing/css/landing_responsive.css" rel="stylesheet">   

    <!-- Custom CSS 
    <link href="<?php bloginfo('template_url');?>/landing/css/simple-sidebar.css" rel="stylesheet">-->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    
</head>

<body <?php body_class(); ?>>
    <!-- MENU PRINCIPAL -->
        <div id="menu"> 
            <div class="container">
                <a href="#" id="logo">Centro de Desarrollo del Liderazgo Educativo</a> 
                <a href="#" id="menu-toggle"> 
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                  </a>
                <div class="box-redes">
                    <a href="https://www.facebook.com/cedlechile" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/CedleChile" target="_blank"><i class="fa fa-twitter"></i></a>
                </div>
                <ul class="sidebar-navs"> 
                    <li>
                        <a href="#seccion_01"> Inicio</a>
                    </li>
                    <li>
                        <a href="#seccion_02">CONFERENCISTAS</a>
                    </li>
                    <li>
                        <a href="#seccion_03">programación</a>
                    </li>
                    <li>
                        <a href="#seccion_04">INSCRIPCIÓN</a> 
                    </li>
                </ul>
                <a href="/index/" id="btn-ir"><i class="fa fa-caret-right"></i> IR DIRECTO A CEDLE</a>
            </div> 
        </div>