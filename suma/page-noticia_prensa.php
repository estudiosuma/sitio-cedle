<?php
/*Template Name: Noticias y Prensa */
get_header(); ?>
<!-- CONTENIDO PRINCIPAL-->
            <div class="col-md-9">
                <!-- Contenido seccion -->
                <div class="section section_noticias  ">
                    <div class="col-xs-6 nav-left">
                        <a href="/index/"><i class="fa fa-arrow-left"></i> Home</a>
                    </div>
                    <div class="col-xs-6 nav-right">
                        <a href="/agenda/">Agenda <i class="fa fa-arrow-right"></i></a>
                    </div>
                    <div class="col-md-12">
                        <!-- SECCION noticias  -->
                        <div class="section-4 box-noticias"> 
                            <div class="text-center top">
                                <img src="<?php bloginfo('template_url');?>/img/icn_noticias.png">
                                <h1>Noticias</h1>
                            </div> 
                            
                            <div class="row">  
                                <div> 
                                    <?php
                                        query_posts(array('showposts' => 1, 'post_type' =>'noticias', 'order'=> 'DESC', 'orderby' => 'order', 'tax_query' => array('relation' => 'AND',array(
                                                                                                                                                            'taxonomy' => 'noticias_categoria',
                                                                                                                                                            'field' => 'term_id',
                                                                                                                                                            'terms' => array(22),
                                                                                                                                                            'operator'  => 'IN'),
                                                                                                                                                            )));
                                        if ( have_posts() ):
                                            $x=0;
                                            $i=0;
                                            
                                            while (have_posts()) :the_post(); 
                                                $x++;
                                                $noticia_no_repetir = $post->ID;
                                                
                                                if (get_field('imagen_detacada')) {
                                                    echo '<div class="col-md-12">';
                                                    echo '
                                                        <div class="box-noticia box-destacada">';
                                                    echo '
                                                    <a href="'.get_permalink().'"><img src="'.get_field('imagen_detacada').'"></a>';
                                                }else{
                                                    echo '<div class="col-md-12 no-photo">';
                                                    echo '
                                                        <div class="box-noticia box-destacada">';
                                                }
                                                echo '        
                                                        <span>'.get_the_date('d/m').'</span>
                                                        <h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>
                                                        <p>'.cortar_palabras(get_field('texto_resumido'),260).'</p>
                                                        <a href="'.get_the_permalink().'" class="ver-mas"><i class="fa fa-arrow-right"></i></a>
                                                    </div>';
                                                echo '</div>';
                                            endwhile;
                                            
                                        endif;
                                        ?>
                                    <?php
                                        query_posts(array('showposts' => 100, 'post_type' =>'noticias','post__not_in' => array($noticia_no_repetir) , 'order'=> 'DESC', 'orderby' => 'order'));
                                        if ( have_posts() ):
                                            $x=0;
                                            $i=0;
                                           
                                            while (have_posts()) :the_post(); 
                                                $x++;
                                                if ($x>3) {
                                                   $class_hide ='contenedor-ocurlto-noticia';
                                                }
                                                if (get_field('imagen_home')) {
                                                    echo '<div class="col-md-4">';
                                                    echo '
                                                        <div class="box-noticia">';
                                                    echo '
                                                    <a href="'.get_permalink().'"><img src="'.get_field('imagen_home').'"></a>';
                                                }else{
                                                    echo '<div class="col-md-4 no-photo">';
                                                    echo '
                                                        <div class="box-noticia">';
                                                }
                                                echo '        
                                                        <span>'.get_the_date('d/m').'</span>
                                                        <h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>
                                                        <p>'.get_field('texto_resumido').'</p>
                                                        <a href="'.get_the_permalink().'" class="ver-mas"><i class="fa fa-arrow-right"></i></a>
                                                    </div>';
                                                echo '</div>';
                                            endwhile;
                                            
                                        endif;
                                        ?>
                                </div>
                            </div>

                            <!-- <a href="#" class="btn btn-primary">Cargar Más</a> -->
                        </div>
                        


                        <!-- SECCION prensa  -->
                        <div class="section-4 box-noticias box-prensa"> 
                            <div class="text-center top">
                                <img src="<?php bloginfo('template_url');?>/img/icn_noticias.png">
                                <h1>Prensa</h1>
                            </div> 
                            <div class="row">  
                                <div> 
<?php
                                query_posts(array('showposts' => 100, 'post_type' =>'prensa', 'meta_key' => 'fecha','order'=> 'DESC', 'orderby' => 'meta_value'));
                                if ( have_posts() ):
                                    $x=0;
                                    $i=0;
                                    $y=0;
                                    $z=0;
                                    $color= '';
                                     // echo $wp_query->post_count; 
                                     $residuo= $wp_query->post_count%3;
                                     $cantidad= $wp_query->post_count;
                                     $cantidad_post=floor($wp_query->post_count/3);
                                     echo '<div class="col-md-4">';   
                                    while (have_posts()) :the_post(); 
                                        $x++;  
                                        $i++;
                                        $y++;
                                        
                                        // $z++;
                                        if ($x==2) {
                                            $color= 'color';
                                            $x=0;
                                        }else{
                                            $color= '';
                                        }   
                                        if ($i>3) {
                                            $class_hide ='contenedor-ocurlto-presa';
                                        }        
                                        
                                        if (get_field('imagen_home')) {
                                            echo '
                                                <div class="box-noticia doble  '.$color.' '.$class_hide.'">';
                                            echo '
                                            <a href="'.get_permalink().'"><img src="'.get_field('imagen_home').'"></a>';
                                        }else{
                                            echo '
                                                <div class="box-noticia '.$color.' '.$class_hide.'">';
                                        }
                                        echo '        
                                                <span>'.get_field('fecha').'</span>
                                                <h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
                                        if (get_field('fuente')) {
                                            echo '<small>Fuente: <strong>'.get_field('fuente').'</strong></small> ';
                                        }

                                        echo    '<a href="'.get_the_permalink().'" class="ver-mas"><i class="fa fa-arrow-right"></i></a>
                                            </div>';
                                        if($residuo==0){
                                            if ($y==$cantidad_post) {
                                                echo '</div>';
                                                if ($i!=$cantidad) {
                                                    echo '<div class="col-md-4">';
                                                }
                                                $y=0;
                                            }
                                        }
                                        if($residuo==1){
                                            if (($y-1)==$cantidad_post) {
                                                echo '</div>';
                                                if ($i!=$cantidad) {
                                                    echo '<div class="col-md-4">';
                                                }
                                                $y=0;
                                            }
                                        }
                                        if($residuo==2){
                                            if (($y-1)==$cantidad_post) {
                                                echo '</div>';
                                                if ($i!=$cantidad) {
                                                    echo '<div class="col-md-4">';
                                                }
                                                $y=0;
                                            }
                                        }
                                        
                                    endwhile;
                                    if ($y!=0) {
                                        echo '</div>';
                                    }
                                   
                                endif;
                                ?>

                                </div>
                            </div>

                            <!-- <a href="#" class="btn btn-primary">Cargar Más</a> -->
                        </div>

                        <a href="#" class="btn_fixed">Prensa <i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                       
                    </div>
                </div> 
<?php get_footer(); ?>
