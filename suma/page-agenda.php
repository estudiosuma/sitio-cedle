<?php
/*Template Name: Agenda*/
get_header(); ?>
<!-- CONTENIDO PRINCIPAL-->
<div class="col-md-9">
	<!-- Contenido seccion -->
	    <div class="section section_agenda ">
	        <div class="col-xs-6 nav-left">
	            <a href="/index/"><i class="fa fa-arrow-left"></i> Home</a>
	        </div>
	        <div class="col-xs-6 nav-right">
	            <a href="/contacto/">Contacto <i class="fa fa-arrow-right"></i></a>
	        </div>
	        <div class="row">
	        <div class="col-md-12">
	            <div class="text-center top">
	                <img src="<?php bloginfo('template_url');?>/img/icn_agenda.png">
	                <h1>Calendario de eventos</h1>
	                <div>
	                    <select id="agenda-mes" class="selectpicker">
	                        <option value="01" <?php if(date('m')==01){echo 'selected';}?> >Enero</option>
	                        <option value="02" <?php if(date('m')==02){echo 'selected';}?> >Febrero</option>
	                        <option value="03" <?php if(date('m')==03){echo 'selected';}?> >Marzo</option>
	                        <option value="04" <?php if(date('m')==04){echo 'selected';}?> >Abril</option>
	                        <option value="05" <?php if(date('m')==05){echo 'selected';}?> >Mayo</option>
	                        <option value="06" <?php if(date('m')==06){echo 'selected';}?> >Junio</option>
	                        <option value="07" <?php if(date('m')==07){echo 'selected';}?> >Julio</option>
	                        <option value="08" <?php if(date('m')==08){echo 'selected';}?> >Agosto</option>
	                        <option value="09" <?php if(date('m')==09){echo 'selected';}?> >Septiembre</option>
	                        <option value="10" <?php if(date('m')==10){echo 'selected';}?> >Octubre</option>
	                        <option value="11" <?php if(date('m')==11){echo 'selected';}?> >Noviembre</option>
	                        <option value="12" <?php if(date('m')==12){echo 'selected';}?> >Diciembre</option>
	                    </select>

	                    <select id="agenda-ano" class="anios">
	                        <option value="2016" selected>2016</option> 
	                        <option value="2015">2015</option> 
	                        <option value="2014">2014</option> 
	                        <option value="2013">2013</option>
	                        <option value="2012">2012</option> 
	                        <option value="2011">2011</option> 
	                        <option value="2010">2010</option> 
	                    </select>
	                </div>
	            </div> 

				<?php 
				    $dias_mes = diasMes();
				    // echo date('dmY').'-'.$dias_mes.date('mY');
				    $inicio_de_mes= date('Ym').'01';
				    $arg=array('showposts' => 100, 'post_type' => 'evento', 'order'=> 'ASC', 'orderby' => 'order','meta_query' => array(
						            'relation' => 'AND',
						            array(
						                'key' => 'fecha_del_evento',
						                'value' => $inicio_de_mes,
						                'compare' =>'>='
						                 ),
						            array(
						                'key' => 'fecha_del_evento',
						                'value' => date('Ym').$dias_mes,
						                'compare' =>'<='
						                 )
						           ));
				    // pr($arg);
				    query_posts($arg);
				    if ( have_posts() ):
				    	// echo '--';
				    	$x=0;
				        while (have_posts()) :the_post(); 
				        	$taxonomy3="evento_tipo";
							$temas='';
							$terms3 = wp_get_post_terms( $post->ID, $taxonomy3 );
                            foreach ($terms3 as $key => $value) {
                            	if ($key>0) {
                            		$temas.=', ';
                            	}
                            	$temas.= $value->name;
                            }
				        	$posisicion[$x]=$post->ID;
				            $date = get_field('fecha_del_evento');
				            $fecha_del_evento=date('j',strtotime($date));
				            // echo $fecha_del_evento;
				            $html[$fecha_del_evento]['id']=$post->ID;
				            if(get_field('imagen_home')){
					            $html[$fecha_del_evento]['img']='  <img src="'.get_field('imagen_home').'">';
					        }else{
					        	$html[$fecha_del_evento]['img']='';
					        }
				            $html[$fecha_del_evento]['small']='<small>'.date('d',strtotime($date)).' '.mes_espanol(date('n',strtotime($date))).', '.date('Y',strtotime($date)).'</small>';
				            $html[$fecha_del_evento]['span']='<span>'.$temas.'</span>';
				            $html[$fecha_del_evento]['h3']='<h3>'.get_the_title().'</h3>';
				            $html[$fecha_del_evento]['p']='<p>'.get_field('descripcion_del_evento').'</p>';
				            if ($date>= date('Ymd')) {
					            if (get_field('activar_boton')) {
				                    if (get_field('tipo_boton')=='modal') {
				                        $html[$fecha_del_evento]['a']='<a href="#"  class="btn btn-primary btn-inscribirme" data-nombre_evento="'.get_the_title().'"">Inscribirme</a>';
				                    }else{
				                        $html[$fecha_del_evento]['a']='<a href="'.get_field('url_boton').'"  target="_blank" class="btn btn-primary " data-nombre_evento="'.get_the_title().'"">'.get_field('texto_de_boton').'</a>';
				                    }
				                }
				            }
				            $x++;                
				        endwhile;
				    endif; ?>
	             <!-- SECCION 8 -->
	            <div class="sections-8 "> 
	                <div class="rows">
	                    <div class="col-md-5 calendario">
	                        <div class="row">
                                <?php 
                                $x=1;
                                $z=0;
                                $y=0;
                                $w=0;
                                for ($i=1; $i <= $dias_mes; $i++) { 
                                    switch ($x) {
                                        case '1':$numero='uno';$x++;break;
                                        case '2':$numero='dos';$x++;break;
                                        case '3':$numero='tres';$x=1;break;
                                    }
                                    if ($html[$i]['id']) {
                                        echo '<a class="col-md-2 activo '.$numero.'"  data-evento="'.$html[$i]['id'].'" href="#">'.$i.'</a>';
                                        if ($w==0 && $z!=0) {
                                        	$w=$i;
                                        }
                                        if ($z==0) {
                                            $z=$i;
                                        }
                                    }else{
                                        echo '<div class="col-md-2 '.$numero.'"  href="#">'.$i.'</div>';
                                    }
                                    $y=$y+2;
                                }
                                $tamano_barra_evento=$y%12;
                                $tamano_barra_evento=12-$tamano_barra_evento;
                                // echo $tamano_barra_evento;
                                if ($tamano_barra_evento<=4) {
                                    $tamano_barra_evento=12;
                                }
                                ?>
                                <div class="col-md-<?php echo $tamano_barra_evento;?> btn-selecciona" href="#">Selecciona un evento para visualizar  ></div> 
                            </div>
	                    </div>
	                    <div class="col-md-7 item-evento">
	                        <!-- aca cargar nuevo evento -->
	                        <?php if($html[$z]['id']){ ?>
	                        	
                                <div class="txt scroll dark" data-mcs-theme="minimal-dark">
                                    <?php echo $html[$z]['small']; ?>
                                    <?php echo $html[$z]['span']; ?>
                                    <?php echo $html[$z]['h3']; ?>
                                    <?php echo $html[$z]['p']; ?>
                                    <?php echo $html[$z]['a']; ?>
                                    <div class="col-md-12 redes-evento-<?php echo $html[$z]['id'];?>" style="margin-bottom: 15px;">
                        			</div>
                                    <div class="nav">
                                    	<?php if($html[$w]['id']){ ?>
                                        <!-- <a href="#" class="evento-anterior"><i class="fa fa-arrow-left"></i></a>  -->
                                        <?php } ?>
                                        <?php if($html[$w]['id']){ ?>
                                        <a href="#" class="evento-posterior" data-evento="<?php echo $html[$w]['id']; ?>"><i class="fa fa-arrow-right"></i></a> 
                                        <?php } ?>
                                    </div>
                                    <?php echo $html[$z]['img']; ?>
                                </div>
                            <?php }else{ ?>
                            	<style type="text/css">
                            	.section_agenda .sections-8 .item-evento {
								    background: #3c3c3c none repeat scroll 0 0;
								    height: 480px;
								    margin-left: -1px;
								    padding: 0;
								}
								.col-md-7.item-evento > p {
								    color: #fff;
								    font-size: 16px;
								    margin-top: 200px;
								    text-align: center;
								}
                            	</style>
                            	<p>No se encuentran eventos para la fecha seleccionada. Elige el mes siguiente para ver nuestros próximos eventos.</p>
                            <?php } ?>
	                        <!--  fin  -->  
	                    </div>
	                </div>
	                <!-- SECCION EVENTOS -->
	                <div class="section-hiden">
	                    <?php
	                    	$x=0;
	                    	if(count($html)>0){
		                        foreach ($html as $key => $value) {
		                            echo '<div id="evento-'.$value['id'].'" >';
			                            
										echo '<div class="txt dark" data-mcs-theme="minimal-dark">';
										echo $html[$key]['small']; 
										echo $html[$key]['span'];
										echo $html[$key]['h3']; 
										echo $html[$key]['p'];
										echo $html[$key]['a'];
										echo '<div class="col-md-12" style="margin-bottom: 15px;"> ';
                    					echo '</div>';
										echo '<div class="nav">';
										if($html[($key-1)]['id']){ 
										echo '<a href="#" class="evento-anterior" data-evento="'.$html[($key-1)]['id'].'"><i class="fa fa-arrow-left"></i></a>';
										}
										 if($html[($key+1)]['id']){ 
										echo '<a href="#" class="evento-posterior" data-evento="'.$html[($key+1)]['id'].'"><i class="fa fa-arrow-right"></i></a> ';
										 } 
										echo '</div>';
										echo "<script type='text/javascript'>
											    $(document).ready(function() {
													  	//modal incribirse
														$( '.btn-inscribirme' ).click(function() {
															$('#modal-inscripcion').modal('show'); 
														  	// return false
														});
												});
											    </script>";
											    echo $html[$key]['img'];
			                            echo '</div>
			                            </div>';
		                        }
		                    }
	                    ?>
	                </div>
	            </div>

	            <!-- FIN SECCION 8 -->
	           
	        </div>
	        </div>
	    </div> 




		<!-- Modal -->
	    <div class="modal fade" id="modal-exito" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	      <div class="modal-dialog" role="document">
	        <div class="modal-content">
	          <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
	          </div>
	          <div class="modal-body">
	            <div class="row">
	                <div class="col-xs-4 text-center">
	                    <i class="fa fa-check icn"></i> 
	                </div>
	                <div class="col-xs-8">
	                    <p>SE HA COMPLETADO <br>TU INSCRIPCIÓN.</p>
	                </div>
	            </div>
	             
	          </div>
	          
	        </div>
	      </div>
	    </div>

	    <!-- Modal -->
	    <div class="modal fade" id="modal-inscripcion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	      <div class="modal-dialog" role="document">
	        <div class="modal-content">
	          <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
	          </div>
	          <div class="modal-body">
	            <h2>FORMULARIO DE INSCRIPCIÓN A <strong>“NOMBRE DEL EVENTO”</strong></h2>
	            <form id="id-formulario-inscribete">
	                <div>
	                    <label>Nombre</label>
	                    <input type="text" id="inputnombre" name="nombre" class="text">
	                    <span class="error">Ingresa Tu Nombre</span>
	                </div> 
	                <div>
	                    <label>Correo</label>
	                    <input type="text" id="inputcorreo" name="correo" class="text">
	                    <span class="error">Ingresa Tu Correo</span>
	                </div>
	                <div>
	                    <label>Asunto</label>
	                    <input type="text" id="inputasunto" name="asunto" class="text">
	                    <span class="error">Ingresa Tu Asunto</span>
	                </div>
	                <div>
	                    <label>Mensaje</label>
	                    <textarea name="mensaje" id="inputmensaje"></textarea>
	                    <span class="error">Ingresa Tu Mensaje</span>
	                </div>
	                <a href="#" id="btn-inscripcion" class="btn btn-primary">Confirmar</a>
	            </form>
	          </div>
	          
	        </div>
	      </div>
	    </div>







<?php get_footer(); ?>
    <script type="text/javascript">
    $(document).ready(function() { 
        $.ajax({
               url: "/wp-content/themes/suma/ajax/redes_evento.php",  
               type: "POST",
               data: {
                        id:'<?php echo $html[$z]['id'];?>'
                },
               async: true,
               cache: false,
               success:function(response,textStatus,xhr,data,callback,result){
                   $('.redes-evento-<?php echo $html[$z]['id'];?>').html(response);
               },
              error: function(){
            }
        });
	});
    </script>