<?php get_header(); 
$id = get_the_ID(); 
$taxonomy="publicaciones_categoria";
$taxonomy2="publicaciones_tipo";
$taxonomy3="publicaciones_tema";
$id_taxonomy_select=0;
$terms = wp_get_post_terms( $id, $taxonomy );
foreach ($terms as $value) {
    if ($value->term_id!=4) {
        $id_taxonomy_select = $value;
    }
}
$terms2 = wp_get_post_terms( $id, $taxonomy2 );
$terms3 = wp_get_post_terms( $id, $taxonomy3 );
$temas='';
foreach ($terms3 as $key => $value) {
    if ($key>0) {
        $temas.=', ';
    }
    $temas.= $value->name;
}
$post_object = get_field('autor');
$autores='';
foreach ($post_object as $key => $value) {
    if ($key>0) {
        $autores.=', ';
    }
    $autores.= $value->post_title;
}

// pr($post_object);
?>
            <!-- CONTENIDO PRINCIPAL-->
            <div class="col-md-9">

                <!-- Contenido seccion -->
                <div class="section single_prensa single_publicacion single row">
                    <div class="col-xs-6 nav-left">
                        <a href="/publicacion/"><i class="fa fa-arrow-left"></i> Publicaciones</a>
                    </div>
                    <div class="col-xs-6 nav-right">
                        <?php $next_post = get_next_post(); ?>
                        <?php if ( is_a( $next_post, 'WP_Post' ) ) {  ?>
                            <a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo get_the_title( $next_post->ID ); ?> <i class="fa fa-arrow-right"></i></a>
                        <?php } ?> 
                    </div> 
                    <div class="col-md-12">
                        <div class="text-center top">
                        <img src="<?php bloginfo('template_url');?>/img/icn_cuaderno.png"> 
                            <h1><?php the_title(); ?></h1>
                        </div>

                        <div class="padre_principal">
                            <?php if(get_field('embed_de_archivo')){ ?>
                            <div class="row plugin">
                                 <div class="col-md-12" id="plugin"> 
                                    <?php echo get_field('embed_de_archivo'); ?>
                                 </div>
                            </div>
                            <?php } ?>
                            <div class="row principal  <?php if (!get_field('texto')) {echo 'onecolumn'; }?>">
                                <div class="col-md-12"> 
                                    <div class="detalle_publicacion">
                                        <p><strong> Fecha:</strong> <?php echo get_the_date('d / m / Y'); ?></p> 
                                        <p><strong> Autor:</strong> <?php echo $autores;?></p> 
                                        <p><strong> Tema:</strong> <?php echo $temas; ?></p> 
                                        <a href="<?php echo get_field('archivo'); ?>"> <?php echo img_taxonomy_publicaciones_tipo($terms2[0]->term_id); ?> Descargar</a>
                                    </div>
                                    <?php echo get_field('texto'); ?>
                                </div> 
                            </div>
                             
                        </div>

                       
                    </div>
                </div> 
<?php get_footer(); ?>