<?php get_header(); 
$id = get_the_ID(); 
if(get_field('imagen_de_fondo')){ ?>
    <style type="text/css">
      .single_noticia {
          background: #efefef url("<?php echo get_field('imagen_de_fondo'); ?>") no-repeat scroll -1px top;
      }
    </style>
<?php } ?>
<style type="text/css">
    .single .fuente {
    background: #fff none repeat scroll 0 0;
    padding: 30px 0px 17px 48px;
    margin-left: -15px;
    margin-right: -15px;
}
</style>
<!-- CONTENIDO PRINCIPAL-->
            <div class="col-md-9">
                <!-- Contenido seccion -->
                <div class="section single_noticia single row">
                    <div class="col-xs-6 nav-left">
                        <a href="/noticias-y-prensa/"><i class="fa fa-arrow-left"></i> Noticias & prensa</a>
                    </div>
                    <div class="col-xs-6 nav-right">
                        <?php $next_post = get_next_post(); ?>
			            <?php if ( is_a( $next_post, 'WP_Post' ) ) {  ?>
			                <a href="<?php echo get_permalink( $next_post->ID ); ?>">Noticia Siguiente<i class="fa fa-arrow-right"></i></a>
			            <?php } ?>
                    </div> 
                    <div class="col-md-12">
                        <div class="text-center top"> 
                            <h1><?php the_title(); ?></h1>
                        </div>

                        <div class="padre_principal">
                            <div class="row principal  ">
                                <div class="col-md-12">
                                    <span><?php echo get_the_date('d/m'); ?></span>
                                    <?php echo get_field('texto'); ?>
                                </div> 
                            </div>
                            <div class="row" style="background: #fff;">
                                <div class="col-md-12"> 
                                <?php 
                                        $images = get_field('galeria');
                                        if( $images ):
                                        //  echo '<pre>';
                                        // print_r($images);
                                        // echo '</pre>';
                                        ?>
                            
                                    <!-- Galeria -->
                                    <style type="text/css">
                                    .cred{margin-top:20px;font-size:11px;}
                                     #galleria{height:100%;width: 100%;}
                                     </style>
                                    <div id="galleria">

                                    <?php
                                     $cont = 0;
                                    foreach( $images as $image ){   ?>
                                        <a href="<?php echo $image['url']; ?>">
                                            <img src="<?php echo $image['sizes']['thumbnail']; ?>" data-big="<?php echo $image['url']; ?>" >
                                        </a>
                                     <?php } ?>
                                     </div>
                            <?php endif; ?>
                                </div>
                            </div>
                            <div class="row fuente">
                                <div class="col-md-6 left fuente-foto-noticia"> 
                                    <p>Foto: <?php echo get_field('fuente_de_la_foto')?></p>
                                </div>
                                <div class="col-md-6 right"><?php
                                    $url=get_permalink( $id );
                                    echo do_shortcode( '[addtoany url="'.$url.'" title="'.get_the_title().'" ]' );
                                     // echo do_shortcode( '[hupso url="'.$url.'"]' );?>
                                </div>
                            </div>
                            <div class="row secundario  ">
                                <?php 
                                disqus_embed('cedle');
                                    
                                    // echo do_shortcode('[fbcomments url="'.$url.'" width="100%" count="off" num="3" countmsg="wonderful comments!"]'); ?>
                            </div>
                        </div>
                    </div>
                </div> 



    <?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function ($){
        // Load the classic theme
        Galleria.loadTheme("http://cedle.cl/wp-content/themes/suma/gallery_plugins/twelve/galleria.twelve.min.js");
        // Initialize Galleria
        Galleria.run('#galleria', {
            imageCrop: true,
            height:  0.5625
        });
        Galleria.ready(function() {
            Galleria.log('Gallery ready', this); // the Galleria instance
        });
});
</script>