<?php
/*Template Name: Nosotros */
get_header(); 
if(get_field('imagen_cabecera')){ ?>
    <style type="text/css">
    	/*.section_nosotros {
    	    background: #efefef url("<?php echo get_field('imagen_cabecera'); ?>") no-repeat scroll -1px top;
    	}*/
    </style>
<?php } ?>
<!-- CONTENIDO PRINCIPAL-->
            <div class="col-md-9">

                <!-- Contenido seccion -->
                <div class="section section_nosotros row">
                    <div class="col-xs-6 nav-left">
                        <a href="/index/"><i class="fa fa-arrow-left"></i> Home</a>
                    </div>
                    <div class="col-xs-6 nav-right">
                         <a href="/lineas-de-trabajo/"> Lineas de trabajo <i class="fa fa-arrow-right"></i></a>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center top"> 
                        	<div class="inner">
	                        	<img src="<?php echo get_field('imagen_cabecera'); ?>">
	                            <h1><?php echo get_field('titulo_de_la_seccion'); ?></h1> 
	                        </div>
                        </div>
                        <div class="row row-cont">
                            <div class="col-md-6 mision ">
                                <h2>MISIÓN</h2>
                                <?php echo get_field('mision'); ?>
                            </div>
                            <div class="col-md-6 vision ">
                                <h2>Visión</h2>
                                <?php echo get_field('vision'); ?>
                            </div>
                        </div>
                        <div class="row row-equipo">
                            <div class="col-md-12  ">
                                <div class="text-center subtop">
                                    <img src="<?php bloginfo('template_url');?>/img/icn_equipo.png">
                                    <h3>Equipo</h3>
                                    <p><?php echo get_field('bajada'); ?></p>
                                </div>
                            </div> 
                            <div class="seccion"  id="box-director"> 
                            	<?php
		                        query_posts(array('showposts' => 1, 'post_type' => 'equipo_cedle', 'order'=> 'ASC', 'orderby' => 'order', 'tax_query' => array('relation' => 'AND',array(
		                        'taxonomy' => 'cedle_categoria',
		                        'field' => 'term_id',
		                        'terms' => array(8),
		                        'operator'  => 'IN'),
		                        )   ));
		                        if ( have_posts() ):
		                            $taxonomy="cedle_categoria";
		                            while (have_posts()) :the_post();
		                                $terms = wp_get_post_terms( $post->ID, $taxonomy );
		                                // pr($terms);
		                                foreach ($terms as $value) {
		                                    // echo $value['term_id'];
		                                    if ($value->term_id!=8 && $value->term_id!=31) {
		                                        $taxonomy_select = $value;
		                                    }
		                                }
		                                echo '	<div class="col-md-4">
				                                    <div class="item effect-sadie">
				                                        <img src="'.get_field('foto_del_miembro').'">
				                                        <div class="info">
				                                            <h4>'.get_the_title().'</h4>
				                                            <h5>'.ucwords(strtolower($taxonomy_select->name)).'</h5>  
				                                            <p>'.get_field('descripcion').'</p>
				                                            <span><i class="fa fa-envelope" aria-hidden="true"></i> '.get_field('email').'</span>
				                                        </div>
				                                    </div>
				                                </div>';
		                            endwhile;
		                        endif;
		                        ?>
                                <div class="clearfix"></div>
                                <hr> 
                            </div>
                            <div class="seccion" id="box-jefes-lineas"> 
                            	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">
                                  

                                  <!-- Wrapper for slides -->
                                  <div class="carousel-inner" role="listbox">
                                    
                            	<?php
                            	$x=0;
                            	$i=0;
		                        query_posts(array('showposts' => 4, 'post_type' => 'equipo_cedle', 'order'=> 'ASC', 'orderby' => 'order', 'tax_query' => array('relation' => 'AND',array(
		                        'taxonomy' => 'cedle_categoria',
		                        'field' => 'term_id',
		                        'terms' => array(10),
		                        'operator'  => 'IN'),
		                        )   ));
		                        if ( have_posts() ):
		                            $taxonomy="cedle_categoria";
		                            while (have_posts()) :the_post();
		                            $x++;
		                            $i++;
		                                $terms = wp_get_post_terms( $post->ID, $taxonomy );
		                                // pr($terms);
		                                foreach ($terms as $value) {
		                                    // echo $value['term_id'];
		                                    if ($value->term_id!=10 && $value->term_id!=31) {
		                                        $taxonomy_select = $value;
		                                    }
		                                }
		                                if ($i==1) {
		                                	$calss_active ='active';
		                                }else{
		                                	$calss_active='';
		                                }
		                                echo '	
				                                    <div class="item '.$calss_active.'">
				                                        <img src="'.get_field('foto_del_miembro').'">
				                                        <div class="info">
				                                            <h4>'.get_the_title().'</h4>
				                                            <h5>'.ucwords(strtolower($taxonomy_select->name)).'</h5>  
				                                            <p>'.get_field('descripcion').'</p>
				                                            <span><i class="fa fa-envelope" aria-hidden="true"></i> '.get_field('email').'</span>
				                                        </div>
				                                    </div>
				                                ';
				                        if($x==2){
				                        	$x=0;
				                        	echo '<div class="clearfix"></div>';
				                        }
		                            endwhile;
		                        endif;
		                        ?>
		                        </div>
		                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>
                                <hr>
                            </div>

                            <div class="seccion"> 
                            	<div id="carousel-example-generic2" class="carousel slide" data-ride="carousel" data-interval="false">
                                   

                                  <!-- Wrapper for slides -->
                                  <div class="carousel-inner" role="listbox">
                                <?php
                            	$x=0;
                            	$i=0;
		                        query_posts(array('showposts' => 6, 'post_type' => 'equipo_cedle', 'order'=> 'ASC', 'orderby' => 'order', 'tax_query' => array('relation' => 'AND',array(
		                        'taxonomy' => 'cedle_categoria',
		                        'field' => 'term_id',
		                        'terms' => array(15),
		                        'operator'  => 'IN'),
		                        )   ));
		                        if ( have_posts() ):
		                            $taxonomy="cedle_categoria";
		                            while (have_posts()) :the_post();
		                            $x++;
		                            $i++;
		                                $terms = wp_get_post_terms( $post->ID, $taxonomy );
		                                // pr($terms);
		                                foreach ($terms as $value) {
		                                    // echo $value['term_id'];
		                                    if ($value->term_id!=15 && $value->term_id!=31) {
		                                        $taxonomy_select = $value;
		                                    }
		                                }
										if ($i==1) {
		                                	$calss_active ='active';
		                                }else{
		                                	$calss_active='';
		                                }

		                                echo '
				                                    <div class="item '.$calss_active.'">
				                                        <img src="'.get_field('foto_del_miembro').'">
				                                        <div class="info">
				                                            <h4>'.get_the_title().'</h4>
				                                            <h5>'.ucwords(strtolower($taxonomy_select->name)).'</h5>  
				                                            <p>'.get_field('descripcion').'</p>
				                                            <span><i class="fa fa-envelope" aria-hidden="true"></i> '.get_field('email').'</span>
				                                        </div>
				                                    </div>
				                                ';
				                        if($x==3){
				                        	$x=0;
				                        	echo '<div class="clearfix"></div>';
				                        }
		                            endwhile;
		                        endif;
		                        ?>
		                        </div>

                                  <!-- Controls -->
                                  <a class="left carousel-control" href="#carousel-example-generic2" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="right carousel-control" href="#carousel-example-generic2" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>
                        </div>
                        





                         <div class="row row-buscar-equipo">
                            <div class="col-md-12">
                                <h3>Buscar Personas</h3>
                            </div>
                            <div class="col-md-6 buscador">
                                <div class="col-xs-6 directorio active">
                                    <a href="#box-directorio" class="btnbuscador"><span class="icono">A</span> Directorio de personas</a>
                                </div>
                                <div class="col-xs-6 palabra ">
                                    <a href="#box-palabra" class="btnbuscador"><i class="fa fa-search" aria-hidden="true"></i> Buscador por palabra</a>
                                </div>
                                <div class="col-md-12 scroll dark" class=" " data-mcs-theme="minimal-dark">
                                    <div class="items" id="box-directorio">
                                         <ul>
                                            <?php
						                        $letra1='';
						                        $letra2='';
						                        $x=0;
						                        $json_miembros='';
						                        $y=0;
						                        query_posts(array('showposts' => 1000, 'post_type' => 'equipo_cedle', 'order'=> 'ASC', 'orderby' => 'title' ));
						                        if ( have_posts() ):
						                            $taxonomy="cedle_categoria";
						                            while (have_posts()) :the_post();
						                            	if(get_field("descripcion")): 
						                            	$terms = wp_get_post_terms( $post->ID, $taxonomy );
						                                // pr($terms);
						                                $taxonomy_select='';
						                                foreach ($terms as $value) {
						                                    // echo $value['term_id'];
						                                    if ($value->term_id!=31 && $value->term_id!=15 && $value->term_id!=8 && $value->term_id!=10) {
						                                        $taxonomy_select = $value;
						                                    }
						                                }
						                                $letra2 = substr(get_the_title(), 0, 1);
						                                // echo $letra2;
						                                if ($letra1 != $letra2 && $x==0) {
						                                	echo '<li><strong>'.strtoupper($letra2).'</strong>'; 
						                                	$letra1 = $letra2;
						                                	$x++;
						                                }else if($letra1 != $letra2){
						                                	echo '</li>
						                                            <li><strong>'.strtoupper($letra2).'</strong>';
						                                    $letra1 = $letra2;
						                                }
						                                if ($y>0) {
						                                	$json_miembros .=',';
						                                }
						                                $json_miembros .= '{"value":"'.get_the_title().'", "data":"'.ucwords(strtolower($taxonomy_select->name)).'", "descripcion":"'.trim(get_field("descripcion")).'", "email":"'.get_field("email").'"}';


						                                $y++;
						                                ?>
						                                <a href="#" data-info_miembro='{"name":"<?php echo get_the_title(); ?>", "cargo":"<?php echo ucwords(strtolower($taxonomy_select->name)); ?>", "descripcion":"<?php echo trim(get_field("descripcion"));?>", "email":"<?php echo get_field("email");?>"}'><?php echo get_the_title(); ?> / <span><?php echo ucwords(strtolower($taxonomy_select->name)); ?></span></a>

													<?php
														endif;
													endwhile;
													echo '</li>';
						                        endif;
								                        ?>
                                             
                                         </ul>
                                    </div> 
                                    <div class="items" id="box-palabra">
                                    	<span id="contenido-mimebros-autocomplete" data-miembros_autocomplete='[<?php echo $json_miembros; ?>]'></span>
                                        <form>
                                            <input type="text" id="autocomplete" name="autocomplete"> 
                                            <span   id="btn_buscar">Buscar</span>
                                        </form>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-md-6 contenido_buscador">
                                <div class="item">
                                    <!--  asi se tiene que cargar la info en el ajax
                                    <h4>Cristian Cox</h4>
                                    <h5>Jefe de Línea</h5>
                                    <p>Investigación y políticas públicas</p>
                                    <span>Cristian.cox@udp.cl</span>
                                    -->
                                </div> 
                            </div>
                         </div>
                    </div>
                </div> 
<?php get_footer(); ?>
