<?php
function acentos($val=''){
$val=str_replace('á', '&aacute;', $val);
$val=str_replace('é', '&eacute;', $val);
$val=str_replace('í', '&iacute;', $val);
$val=str_replace('ó', '&oacute;', $val);
$val=str_replace('ú', '&uacute;', $val);
$val=str_replace('Á', '&Aacute;', $val);
$val=str_replace('É', '&Eacute;', $val);
$val=str_replace('Í', '&Iacute;', $val);
$val=str_replace('Ó', '&Oacute;', $val);
$val=str_replace('Ú', '&Uacute;', $val);
$val=str_replace('ñ', '&ntilde;', $val);
$val=str_replace('Ñ', '&Ntilde;', $val);
return $val;
}
if(isset($_POST['email'])) { //FIXME: Si viene vacío igual entra a este if
//FIXME:  Que pasa con los parámetros que no están seteados allá abajo
$email_to = "Chile Alimentos <mariana@estudiosuma.cl>";
$email_subject = 'Chile Alimentos';

$email_message = "Hemos recíbido la siguiente información proveniente de La Mensajería a travez del formulario de contacto :<br/><br/>";

$email_message .= "<bold>Datos generales de la Empresa</bold> <br/>";
$email_message .= "Razon Social: " . acentos($_POST['razonSocial']). "<br/>";
$email_message .= "R.U.T: " . acentos($_POST['rut']) . "<br/>";
$email_message .= "Teléfono: " . acentos($_POST['telefono']) . "<br/>";
$email_message .= "Fax: " . acentos($_POST['fax']) . "<br/>";
$email_message .= "Sitio Web: " . acentos($_POST['sitioWeb']) . "<br/>";
$email_message .= "E-mail: " . $_POST['email'] . "<br/>";
$email_message .= "Domicilio Comercial: " . acentos($_POST['domicilioComercial']) . "<br/>";
$email_message .= "Región: " . acentos($_POST['region']) . "<br/>";
$email_message .= "<bold>Datos de la Administración</bold> <br/>";
$email_message .= "Gerente General: " . acentos($_POST['gerenteGeneral']);
$email_message .= " email: " . acentos($_POST['emailGerenteGeneral']) . "<br/>";
$email_message .= "Secretaria: " . acentos($_POST['secretaria']);
$email_message .= " email: " . acentos($_POST['emailGerenteGeneral']) . "<br/>";
$email_message .= "Gerente Comercial: " . acentos($_POST['gerenteComercial']);
$email_message .= " email: " . acentos($_POST['emailGerenteGeneral']) . "<br/>";
$email_message .= "Gerente Finanzas: " . acentos($_POST['gerenteFinanzas']);
$email_message .= " email: " . acentos($_POST['emailGerenteGeneral']) . "<br/>";
$email_message .= "Domicilio Comercial: " . acentos($_POST['domcilioComercial2']);
$email_message .= " email: " . acentos($_POST['emailDomcilioComercial']) . "<br/>";
$email_message .= "Gerente Produccion/Planta: " . acentos($_POST['gerenteProduccion']);
$email_message .= " email: " . acentos($_POST['emailGerenteProduccion']) . "<br/>";
$email_message .= "Encargado de Fletes: " . acentos($_POST['encargadoFletes']);
$email_message .= " email: " . acentos($_POST['emailEncargadoFletes']) . "<br/>";
$email_message .= "Dirección: " . acentos($_POST['direccion']);
$email_message .= " email: " . acentos($_POST['emailDireccion']) . "<br/>";
$email_message .= "<bold>Datos Productos Elaborados</bold> <br/>";
$email_message .= "Producto: " . acentos($_POST['producto']). "<br/>";
$email_message .= "Marca Registrada: " . acentos($_POST['marca']) . "<br/>";
$email_message .= "Ventas en US $ año anterior: " . acentos($_POST['ventasUS']) . "<br/>";
$email_message .= "Ventas en Toneladas: " . acentos($_POST['ventasTonelada']) . "<br/>";
$email_message .= "Otra: " . acentos($_POST['otra']) . "<br/>";
$email_message .= "Año Anterior: " . acentos($_POST['anoAnterior']) . "<br/>";
$email_message .= "<bold>Observaciones</bold> <br/>";
$email_message .= "Observaciones: " . acentos($_POST['observaciones']) . "<br/><br/>";


// Ahora se envía el e-mail usando la función mail() de PHP
$headers = "Content-Type: text/html; charset=UTF-8";
$headers .= "From: No Responder <mariana@estudiosuma.cl> \r\n". "X-Mailer: PHP/" . phpversion();



if(@mail($email_to, $email_subject, $email_message, $headers)) {
	echo "Mensaje Enviado con exito";
} else {
	echo "Hubo un error";
}

}

?>
