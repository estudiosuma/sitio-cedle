<?php get_header(); 
$id = get_the_ID(); 



if(get_field('imagen_de_fondo')){ ?>
    <style type="text/css">
      .single_columna {
          background: #efefef url("<?php echo get_field('imagen_de_fondo'); ?>") no-repeat scroll -1px top;
      }
    </style>
<?php } ?>
 <!-- CONTENIDO PRINCIPAL-->
            <div class="col-md-9">
                <!-- Contenido seccion -->
                <div class="section single_columna single row">
                    <div class="col-xs-6 nav-left">
                        <a href="/actualidad/"><i class="fa fa-arrow-left"></i> Actualidad </a>
                    </div>
					<div class="col-xs-6 nav-right">
                    <?php $next_post = get_next_post(); ?>
		            <?php if ( is_a( $next_post, 'WP_Post' ) ) {  ?>
		                <a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo get_the_title( $next_post->ID ); ?> <i class="fa fa-arrow-right"></i></a>
		            <?php } ?> 
		            </div> 
                    <div class="col-md-12">
                        <div class="text-center top"> 
                            <h1><?php the_title(); ?></h1>
                        </div>

                        <div class="padre_principal">
                            <?php if(get_field('embed')){ ?>
                            <div class="row plugin">
                                 <div class="col-md-12" id="plugin"> 
                                    <?php echo get_field('embed'); ?>
                                 </div>
                            </div>
                            <?php } ?>
                            <div class=" principal   row">
                                <div class="col-md-12">
                                    <div class="nombre">
                                        <?php 
                                        $taxonomy="autor_normativa";
                                        $terms = wp_get_post_terms( $post->ID, $taxonomy );
                                        // pr($terms);
                                        ?>
                                        
                                        <div class="col-xs-5 ">
                                        <?php if (get_field('foto_de_autor',$taxonomy.'_'.$terms[0]->term_id)) { 
                                            echo '<img src="'.get_field('foto_de_autor',$taxonomy.'_'.$terms[0]->term_id).'" data-pin-nopin="true">';
                                            }
                                            ?>
                                        </div>
                                        <div class="col-xs-7">
                                            Normativa de : <strong><?php echo $terms[0]->name; ?></strong>
                                            <p><?php echo $terms[0]->description; ?> </p>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                    </div>

                                    <?php echo get_field('texto'); ?>

                                    <?php if(get_field('descargable')){ 
                                            echo '<p>El siguiente material lo puedes descargar e imprimir para hacerte un cuadernillo o llevarlo a  tus reuniones de equipo directivo. <a class="btn-descarga-descargable" href="'.get_field('descargable').'">Clic aquí.</a></p>';
                                        } ?>
                                </div> 
                            </div>
                            <div class="row fuente">
                                <div class="col-md-6 left">
                                    <!-- <p>Fuente: <strong><?php echo get_field('fuente'); ?></strong></p>
                                    <a target="_blank" href="<?php echo get_field('url_fuente'); ?>">Leer desde <?php echo get_field('fuente'); ?> ></a> -->
                                    <p>Esta sección es presentada gracias a la colaboración de Fundación SEPEC / Sercoop y su equipo jurídico. Para mayor información visitar <a target="_blank" href="http://www.fundacionsepec.org/">www.fundacionsepec.org</a></p>
                                </div>
                                <div class="col-md-6 right">
                                    <?php
                                    $url=get_permalink( $id );
                                    echo do_shortcode( '[addtoany url="'.$url.'" title="'.get_the_title().'" ]' );?>
                                </div>
                            </div>
                            <div class="row secundario  faceboock-comentarios">
                                <!-- <div> -->
                                    <?php 
                                    disqus_embed('cedle');
                                    // echo do_shortcode('[fbcomments url="'.$url.'" width="100%" count="off" num="3" countmsg="wonderful comments!"]'); ?>
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                </div> 
<?php get_footer(); ?>
<style type="text/css">
    .single .fuente .left a {
    font-size: 12px;
    letter-spacing: 1px;
}
.single .fuente .left p {
    font-size: 15px;
}
</style>