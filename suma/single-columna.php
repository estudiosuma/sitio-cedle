<?php get_header(); 
$id = get_the_ID(); 
if(get_field('imagen_de_fondo')){ ?>
    <style type="text/css">
      .single_columna {
          background: #efefef url("<?php echo get_field('imagen_de_fondo'); ?>") no-repeat scroll -1px top;
      }
    </style>
<?php } ?>
 <!-- CONTENIDO PRINCIPAL-->
            <div class="col-md-9">

                <!-- Contenido seccion -->
                <div class="section single_columna single row">
                    <div class="col-xs-6 nav-left">
                        <a href="/actualidad/"><i class="fa fa-arrow-left"></i> Actualidad </a>
                    </div>
					<div class="col-xs-6 nav-right">
                    <?php $next_post = get_next_post(); ?>
		            <?php if ( is_a( $next_post, 'WP_Post' ) ) {  ?>
		                <a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo get_the_title( $next_post->ID ); ?> <i class="fa fa-arrow-right"></i></a>
		            <?php } ?> 
		            </div> 
                    <div class="col-md-12">
                        <div class="text-center top"> 
                            <h1><?php the_title(); ?></h1>
                        </div>

                        <div class="padre_principal row">
                            <div class=" principal  ">
                                <div class="col-md-12">
                                    <div class="nombre">
                                        <?php 
                                        $taxonomy="columna_columnista";
                                        $terms = wp_get_post_terms( $post->ID, $taxonomy );
                                        // pr($terms);
                                        ?>
                                        
                                        <div class="col-xs-5 ">
                                        <?php if (get_field('foto_de_columnista',$taxonomy.'_'.$terms[0]->term_id)) { 
                                            echo '<img src="'.get_field('foto_de_columnista',$taxonomy.'_'.$terms[0]->term_id).'" data-pin-nopin="true">';
                                            }
                                            ?>
                                        </div>
                                        <div class="col-xs-7">
                                            Columna de : <strong><?php echo $terms[0]->name; ?></strong>
                                            <p><?php echo $terms[0]->description; ?> </p>
                                        </div>
                                        <div class="clearfix"></div>
                                        
                                    </div>

                                    <?php echo get_field('texto'); ?>
                                </div> 
                            </div>
                            <div class="row fuente">
                                <div class="col-md-6 left">
                                </div>
                                <div class="col-md-6 right">
                                    <?php
                                    $url=get_permalink( $id );
                                    echo do_shortcode( '[addtoany url="'.$url.'" title="'.get_the_title().'" ]' );?>
                                </div>
                            </div>
                            <div class="row secundario  faceboock-comentarios">
                                <!-- <div> -->
                                    <?php  disqus_embed('cedle');
                                    // echo do_shortcode('[fbcomments url="'.$url.'" width="100%" count="off" num="3" countmsg="wonderful comments!"]'); 
                                    //comments_template();?>
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                </div> 
<?php get_footer(); ?>
