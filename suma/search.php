<?php
get_header(); 

// pr($_GET);
if (count($_GET['filtro_formulario'])>0) {
    foreach ($_GET['filtro_formulario'] as $key => $value) {
        $checkbox_filtro[$value]=1;
    }
    $buscador_generico=0;
}else{
    $buscador_generico=1;
}
$s = (get_query_var('s')) ? get_query_var('s') : "" ;
?>
<!-- CONTENIDO PRINCIPAL-->
<div class="col-md-9">

    <!-- Contenido seccion -->
    <div class="section section_busqueda">
        <img src="<?php bloginfo('template_url');?>/img/icn_buscar.png">
        <h1>RESULTADOS DE BÚSQUEDA</h1>
        
        <div class="box-buscador">
            <form  action="<?php echo home_url(); ?>"  id="form_search_page" method="GET">
                <div class="boxs"> 
                    <input name="s" type="text" class="text-buscar" value="<?php echo $_GET['s']; ?>"> 
                    <a href="#" class="btn-buscar btn-search-page"><i class="fa fa-search"></i></a>
                    
                </div> 
                <div class="clearfix"> </div>
                <div class="option"> 
                    <label>
                        <input name="filtro_formulario[]"  <?php if($checkbox_filtro['persona']==1){echo 'checked';}?> value="persona" type="checkbox">
                        <i class="fa fa-user"></i>
                        Personas
                    </label>
                    <label>
                        <input name="filtro_formulario[]"  <?php if($checkbox_filtro['publicacion']==1){echo 'checked';}?> value="publicacion" type="checkbox">
                        <i class="fa fa-download"></i>
                        Publicaciones
                    </label>
                    <label>
                        <input name="filtro_formulario[]" <?php if($checkbox_filtro['noticia_prensa']==1){echo 'checked';}?>  value="noticia_prensa" type="checkbox">
                        <i class="fa fa-file-text-o"></i>
                        Noticias & prensa
                    </label> 
                </div>
            </form>
        </div>

        <div class="row">
            <?php if($checkbox_filtro['publicacion']==1){ ?>
            <div class="col-md-12 box-section">
                <h2>Publicaciones donde se a mencionado: <?php echo $_GET['s'];?></h2>
                <?php
                        
                        query_posts(array('showposts' => 1000, 'post_type' => 'equipo_cedle', 'order'=> 'ASC', 'orderby' => 'title','s' => $s ));
                        if ( have_posts() ):
                            $taxonomy="cedle_categoria";
                            while (have_posts()) :the_post();
                                $autores[]=$post->ID;
                            endwhile;
                        endif;
                         wp_reset_postdata();
                        
                        $query = array(
                            'post_type' => 'publicaciones',
                            'order' => 'DESC',
                            'orderby' => 'order',
                            's' => $s
                        );
                        query_posts($query);
                        if ( ! have_posts()) {
                            wp_reset_postdata();
                            $query = array(
                                'post_type' => 'publicaciones' ,
                                'order' => 'DESC',
                                'orderby' => 'order',
                                'meta_query' => array(
                                        'relation' => 'AND',
                                        array(
                                            'key' => 'texto',
                                            'value' => $s,
                                            'compare' =>'LIKE'
                                             )
                                       )
                            );
                            query_posts($query);
                        }
                        if ( ! have_posts()) {
                            wp_reset_postdata();
                            $query = array(
                                'post_type' => 'publicaciones' ,
                                'order' => 'DESC',
                                'orderby' => 'order',
                                'meta_query' => array(
                                        'relation' => 'AND',
                                        array(
                                            'key' => 'autor',
                                            'value' => '"'.$autores[0].'"',
                                            'compare' =>'LIKE'
                                             )
                                       )
                            );
                            query_posts($query);
                        }
                        // pr($query);s
                        
                        if ( have_posts() ):
                            while (have_posts()) { the_post(); ?>
                                <div class="col-md-12 item">  
                                    <?php if(get_field('imagen_home')){?><img src="<?php echo get_field('imagen_home'); ?>"><?php } ?>
                                    <h3><?php the_title(); ?></h3>
                                    <p><?php echo get_field('texto_resumido'); ?></p>
                                    <a href="<?php the_permalink();?>" class="ver-mas">Ver más</a>
                                    <div class="clearfix"></div>
                                </div>
                <?php       }
                        else:?>
                        <div class="col-md-12 numeros_resultados">
                            <p>Se han encontrado 0 resultado con la palabra(s) “<?php echo $_GET['s'];?>”</p>
                        </div> 
                <?php 
                        endif;
                        wp_reset_postdata();
                        ?>
            </div>
            <?php } ?>
            <?php if($buscador_generico==1 || $checkbox_filtro['noticia_prensa']==1){ ?>
            <div class="col-md-12 box-section">
                <h2>Noticias y Prensa se menciona: <?php echo $_GET['s'];?></h2>
                <?php
                        
					    
						$query = array(
							'post_type' => array('noticias','prensa'),
							'order' => 'DESC',
                            'orderby' => 'order',
							's'	=> $s
						);
                        query_posts($query);
                        if ( ! have_posts()) {
                            wp_reset_postdata();
                            $query = array(
                                'post_type' => array('noticias','prensa'),
                                'order' => 'DESC',
                                'orderby' => 'order',
                                'meta_query' => array(
                                        'relation' => 'AND',
                                        array(
                                            'key' => 'texto',
                                            'value' => $s,
                                            'compare' =>'LIKE'
                                             )
                                       )
                            );
                            query_posts($query);
                        }
                        if ( have_posts() ):
                            while (have_posts()) { the_post(); ?>
    			                <div class="col-md-12 item">  
    			                    <?php if(get_field('imagen_home')){?><img src="<?php echo get_field('imagen_home'); ?>"><?php } ?>
    			                    <h3><?php the_title(); ?></h3>
    			                    <p><?php echo get_field('texto_resumido'); ?></p>
    			                    <a href="<?php the_permalink();?>" class="ver-mas">Ver más</a>
    			                    <div class="clearfix"></div>
    			                </div>
                <?php       }
                        else:?>
                        <div class="col-md-12 numeros_resultados">
                            <p>Se han encontrado 0 resultado con la palabra(s) “<?php echo $_GET['s'];?>”</p>
                        </div> 
                <?php 
                        endif;
                        wp_reset_postdata();
                        ?>
            </div>
            <?php } ?>
        </div>
    </div> 
    <?php if($checkbox_filtro['persona']==1){ ?>
    <div class="section section_nosotros row">
        <div class="row row-equipo">
            <div class="col-md-12 box-section">
                <h2>Personas con coincidencia: <?php echo $_GET['s'];?></h2>
                <div class="row row-buscar-equipo">
                    <div class="col-md-12">
                        <h3>Buscar Personas</h3>
                    </div>
                    <div class="col-md-6 buscador">
                        <div class="col-xs-6 directorio active">
                            <a href="#box-directorio" class="btnbuscador"><span class="icono">A</span> Directorio de personas</a>
                        </div>
                        <div class="col-xs-6 palabra ">
                            <a href="#box-palabra" class="btnbuscador"><i class="fa fa-search" aria-hidden="true"></i> Buscador por palabra</a>
                        </div>
                        <div class="col-md-12 ">
                            <div class="items" id="box-directorio">
                                 <ul>
                                    <?php
                                        $letra1='';
                                        $letra2='';
                                        $x=0;
                                        $json_miembros='';
                                        $y=0;
                                        wp_reset_postdata();
                                        

                                        query_posts(array('showposts' => 1000, 'post_type' => 'equipo_cedle', 'order'=> 'ASC', 'orderby' => 'title','s' => $s ));
                                        if ( have_posts() ):
                                            $taxonomy="cedle_categoria";
                                            while (have_posts()) :the_post();
                                                $terms = wp_get_post_terms( $post->ID, $taxonomy );
                                                // pr($terms);
                                                foreach ($terms as $value) {
                                                    // echo $value['term_id'];
                                                    if ($value->term_id!=15 && $value->term_id!=8 && $value->term_id!=10) {
                                                        $taxonomy_select = $value;
                                                    }
                                                }
                                                $letra2 = substr(get_the_title(), 0, 1);
                                                // echo $letra2;
                                                if ($letra1 != $letra2 && $x==0) {
                                                    echo '<li><strong>'.strtoupper($letra2).'</strong>'; 
                                                    $letra1 = $letra2;
                                                    $x++;
                                                }else if($letra1 != $letra2){
                                                    echo '</li>
                                                            <li><strong>'.strtoupper($letra2).'</strong>';
                                                    $letra1 = $letra2;
                                                }
                                                if ($y>0) {
                                                    $json_miembros .=',';
                                                }
                                                $json_miembros .= '{"value":"'.get_the_title().'", "data":"'.ucwords(strtolower($taxonomy_select->name)).'", "descripcion":"'.trim(get_field("descripcion")).'", "email":"'.get_field("email").'"}';


                                                $y++;
                                                ?>
                                                <a href="#" data-info_miembro='{"name":"<?php echo get_the_title(); ?>", "cargo":"<?php echo ucwords(strtolower($taxonomy_select->name)); ?>", "descripcion":"<?php echo trim(get_field("descripcion"));?>", "email":"<?php echo get_field("email");?>"}'><?php echo get_the_title(); ?> / <span><?php echo ucwords(strtolower($taxonomy_select->name)); ?></span></a>

                                            <?php
                                            endwhile;
                                            echo '</li>';
                                        else:?>
                                            <li>
                                                <a href="#" data-info_miembro='{"name":"Sin coincidencias ", "cargo":"", "descripcion":"", "email":""}'>Sin coincidencias </span></a>
                                            </li> 
                                    <?php 
                                        endif;
                                        wp_reset_postdata();
                                                ?>
                                     
                                 </ul>
                            </div> 
                            <div class="items" id="box-palabra">
                                <span id="contenido-mimebros-autocomplete" data-miembros_autocomplete='[<?php echo $json_miembros; ?>]'></span>
                                <form>
                                    <input type="text" id="autocomplete" name="autocomplete"> 
                                    <span   id="btn_buscar">Buscar</span>
                                </form>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-6 contenido_buscador">
                        <div class="item">
                            <!--  asi se tiene que cargar la info en el ajax
                            <h4>Cristian Cox</h4>
                            <h5>Jefe de Línea</h5>
                            <p>Investigación y políticas públicas</p>
                            <span>Cristian.cox@udp.cl</span>
                            -->
                        </div> 
                    </div>
                 </div>
            </div>
        </div>
    </div>
            <?php } ?>



<?php 
// pr($autores);
// pr($s); 
get_footer(); ?>
