            </div>
        </div>

    </div>
    <!-- /#wrapper -->
<!-- Modal -->
                <div class="modal fade" id="modal-bienvenido" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                      </div>
                      <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12  ">
                                 <h1><strong></strong></h1>
                                 <p>Para completar tu suscripción debes elegir tu cargo, para recibir información dedicada.</p>
                            </div>
                            <form>
                                <div class="col-xs-6  ">
                                    <input type="radio" id="opcion1" name="cargo" value="Director/a"><label for="opcion1">  Director/a</label>
                                </div>
                                <div class="col-xs-6  ">
                                    <input type="radio" id="opcion2" name="cargo" value="Docente"><label for="opcion2">  Docente</label>
                                </div>
                                <div class="col-xs-6  ">
                                    <input type="radio" id="opcion3" name="cargo" value="Jefe/a de UTP"><label for="opcion3">  Jefe/a de UTP</label>
                                </div>
                                <div class="col-xs-6  ">
                                    <input type="radio" id="opcion4" name="cargo" value="Otro"><label for="opcion4">  Otro</label>
                                </div>
                                <div class="col-xs-6  ">
                                    <input type="radio" id="opcion5" name="cargo" value="Inspector/a"><label for="opcion5">  Inspector/a</label>
                                </div>
                            </form>  
                            <div class="col-xs-12 text-center  ">
                                <a href="#" id="btn-confirmar-bienvenido" class="btn btn-primary">Confirmar</a>
                            </div>
                        </div>
                         
                      </div>
                      
                    </div>
                  </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="modal-exito" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                      </div>
                      <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <i class="fa fa-check icn"></i> 
                            </div>
                            <div class="col-xs-8">
                                <p>SE HA COMPLETADO <br>TU INSCRIPCIÓN.</p>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
    <!-- jQuery -->
    <script src="<?php bloginfo('template_url');?>/js/jquery.js"></script>
    <script src="<?php bloginfo('template_url')?>/gallery_plugins/galleria-1.4.2.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/typed.min.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/scrollreveal.min.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/bootstrap-select.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/jquery.autocomplete.js"></script>
    <script src='<?php bloginfo('template_url');?>/js/jquery.shuffle.modernizr.js' type='text/javascript'></script>
    <script src='<?php bloginfo('template_url');?>/js/jquery.shuffle.js' type='text/javascript'></script>
    
    <script src="<?php bloginfo('template_url');?>/js/funciones.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/validations.js"></script>

    



<?php

	wp_footer();
?>
</body>
</html>
