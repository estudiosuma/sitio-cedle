<?php
/*Template Name: Landing */
get_header('landing'); ?>
<style type="text/css">
    .box-programacion .ciudades::after {
        background:none;
    }
</style>
	<div id="seccion_01" class="box-inicio">
        <div class="container">
            <p>En el marco del lanzamiento del Centro de Desarrollo de Liderazgo Educativo (CEDLE), conformado por la Universidad Diego Portales, la Universidad Alberto Hurtado, la Universidad de Talca, la Universidad Católica de Temuco, en colaboración con el Ministerio de Educación y la Escuela de Graduados de Educación UC Berkeley, deseamos invitarle a:</p>
            <h1>Seminario Internacional CEDLE</h1>
            <h2>Nuevas tendencias <br>del liderazgo pedagógico</h2>
            <a href="#seccion_04">Inscribete</a>
        </div>
    </div>

    <div id="seccion_02" class="box-conferencistas">
        <h3>Conferencistas</h3>

        <div class="container">
            <div class="box row">
                <div class="col-sm-6">
                    <div class="item">
                        <blockquote>“Los directivos escolares y el fomento de las comunidades de aprendizaje entre docentes”</blockquote>
                        <img src="<?php bloginfo('template_url');?>/landing/images/cnt_judith.png">
                        <h4>Judith Warren Little </h4>
                        <span>Doctora en Sociología</span>
                        <p> Académica y Ex Decana de la Escuela de <br>Graduados en Educación UC Berkeley.</p>
                        <strong>Santiago</strong>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="item">
                        <blockquote>“Los directivos escolares y el uso de datos para la mejora de su establecimiento: La experiencia de California”</blockquote>
                        <img src="<?php bloginfo('template_url');?>/landing/images/cnt_rebecca.png">
                        <h4>Rebecca Cheung </h4>
                        <span>Doctora en Educación</span>
                        <p>Académica y Directora del Instituto de <br> Liderazgo Directivo, UC Berkeley</p>
                        <strong>santiago / talca / temuco</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="seccion_03" class="box-programacion">
        <h3>Información</h3>
        <h4>Selecciona el seminario al cual deseas asistir </h4>
        <p><strong>Entrada es liberada </strong>(en todas las ciudades).</p>

        <div class="container">
            <div class="ciudades">
                <a href="#santiago" class="activo">Santiago</a>
                <!-- <a href="#talca" class="medio">Talca</a> -->
                <!-- <a href="#temuco">Temuco</a> -->
            </div>
        

        <div class="itemciudad" id="santiago"> 
            <div class=" box-datos-principales">
                <div class="col-xs-4 left">
                    <strong>Fecha</strong>
                    <p>13/05/2016</p>
                </div>
                <div class="col-xs-4 center">
                    <strong>Lugar</strong>
                    <p>Auditorio Biblioteca Nicanor Parra, UDP. </p>
                    <!-- <p>, UDP.</p> -->
                    <p> Vergara 324, Santiago.</p>
                </div>
                <div class="col-xs-4 right">
                    <strong>Horario</strong>
                    <p>09:00 - 13:00 hrs</p>
                </div>
                <div class="clearfix"></div>
            </div>


            <div class="box-programacion">
                 <h4>Programación</h4> 
                <div class="list-programacion  "> 
                    <div class="col-xs-5 left">
                        <p>09:00 / 09:30</p>
                    </div>
                    <div class="col-xs-7 right">
                        <h5>Inscripciones</h5>
                        <!-- <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit </p> -->
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-5 left">
                        <p>09:30 / 10:00</p>
                    </div>
                    <div class="col-xs-7 right">
                        <h5>Palabras Autoridades</h5>
                        <!-- <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit </p> -->
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-5 left">
                        <p>10:00 / 12:00 </p>
                    </div>
                    <div class="col-xs-7 right">
                        <h5>Presentaciones </h5>
                        <div>
                            <blockquote> “Los directivos escolares y el fomento de las comunidades de aprendizaje entre docentes”</blockquote>
                            <span>Judith Warren Little</span>
                        </div>
                        <div>
                            <blockquote> “Los directivos escolares y el uso de datos para la mejora de su establecimiento: La experiencia de California”</blockquote>
                            <span> Rebecca Cheung</span>
                        </div>
                        

                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-5 left">
                        <p>12:00 / 13:00 </p>
                    </div>
                    <div class="col-xs-7 last right">
                        <h5>ronda de preguntas</h5> 
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>

        <div class="itemciudad" id="talca"> 
            <div class=" box-datos-principales">
                <div class="col-xs-4 left">
                    <strong>Fecha</strong>
                    <p>11/05/2016</p>
                </div>
                <div class="col-xs-4 center">
                    <strong>Lugar</strong>
                    <p>Aula Magna Bicentenario, Espacio Bicentenario </p>
                    <p>Campus Lircay, Universidad de Talca</p>
                    <p> Avenida Lircay s/nº , Talca.</p>
                </div>
                <div class="col-xs-4 right">
                    <strong>Horario</strong>
                    <p>09:00 - 13:00 hrs</p>
                </div>
                <div class="clearfix"></div>
            </div>


            <div class="box-programacion">
                 <h4>Programación</h4> 
                <div class="list-programacion  list-alargado"> 
                    <div class="col-xs-5 left">
                        <p>09:30 / 10:00</p>
                    </div>
                    <div class="col-xs-7 right">
                        <h5>Inscripciones</h5>
                        <!-- <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit </p> -->
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-5 left">
                        <p>10:00 / 10:30</p>
                    </div>
                    <div class="col-xs-7 right">
                        <h5>Palabras Autoridades</h5>
                        <!-- <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit </p> -->
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-5 left">
                        <p>10:30 / 11:30 </p>
                    </div>
                    <div class="col-xs-7 right">
                        <h5>Presentación </h5>
                        <div>
                            <blockquote> “Los directivos escolares y el uso de datos para la mejora de su establecimiento: La experiencia de California”</blockquote>
                            <span> Rebecca Cheung</span>
                        </div>
                        <!-- <div>
                            <blockquote> “Los directivos escolares y el fomento de las comunidades de aprendizaje entre docentes”</blockquote>
                            <span> Rebecca Cheung</span>
                        </div>
                         -->

                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-5 left">
                        <p>11:30 / 12:30 </p>
                    </div>
                    <div class="col-xs-7 last right">
                        <h5>Panel de Expertos</h5> 
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-5 left">
                        <p>12:30 / 13:00 </p>
                    </div>
                    <div class="col-xs-7 last right">
                        <h5>Ronda de preguntas</h5> 
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div> 

        <div class="itemciudad" id="temuco"> 
            <div class=" box-datos-principales">
                <div class="col-xs-4 left">
                    <strong>Fecha</strong>
                    <p>10/05/2016</p>
                </div>
                <div class="col-xs-4 center">
                    <strong>Lugar</strong>
                    <p>Aula Magna </p>
                    <p>Campus San Francisco, Universidad Católica de Temuco.</p>
                    <p> Manuel Montt 56, Temuco.</p>
                </div>
                <div class="col-xs-4 right">
                    <strong>Horario</strong>
                    <p>09:00 - 13:00 hrs</p>
                </div>
                <div class="clearfix"></div>
            </div>


            <div class="box-programacion">
                 <h4>Programación</h4> 
                <div class="list-programacion  list-alargado"> 
                    <div class="col-xs-5 left">
                        <p>09:30 / 10:00</p>
                    </div>
                    <div class="col-xs-7 right">
                        <h5>Inscripciones</h5>
                        <!-- <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit </p> -->
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-5 left">
                        <p>10:00 / 10:30</p>
                    </div>
                    <div class="col-xs-7 right">
                        <h5>Palabras Autoridades</h5>
                        <!-- <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit </p> -->
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-5 left">
                        <p>10:30 / 11:30 </p>
                    </div>
                    <div class="col-xs-7 right">
                        <h5>Presentación </h5>
                        <div>
                            <blockquote> “Los directivos escolares y el uso de datos para la mejora de su establecimiento: La experiencia de California”</blockquote>
                            <span> Rebecca Cheung</span>
                        </div>
                        <!-- <div>
                            <blockquote> “Los directivos escolares y el fomento de las comunidades de aprendizaje entre docentes”</blockquote>
                            <span> Rebecca Cheung</span>
                        </div>
                         -->

                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-5 left">
                        <p>11:30 / 12:30 </p>
                    </div>
                    <div class="col-xs-7 last right">
                        <h5>Panel de Expertos</h5> 
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-5 left">
                        <p>12:30 / 13:00 </p>
                    </div>
                    <div class="col-xs-7 last right">
                        <h5>Ronda de preguntas</h5> 
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
        
        </div>

       

    </div>

    <div id="seccion_04" class="box-inscripcion">
        <h5>FORMULARIO DE INSCRIPCIÓN</h5>

        <div class="body"> 
            <form id="id-formulario-landing">
                <div>
                    <label>Nombre</label>
                    <input type="text" id="nombre" name="nombre" class="text">
                    <span class="error">Ingresa Tu Nombre</span>
                </div> 
                <div>
                    <label>Rut</label>
                    <input type="text" id="rut" name="rut" class="text">
                    <span class="error">Ingresa Tu Rut</span>
                </div>
                <div>
                    <label>Cargo</label>
                    <input type="text" id="cargo" name="cargo" class="text">
                    <span class="error">Ingresa Tu Cargo</span>
                </div>
                <div>
                    <label>Institución</label>
                    <input type="text" id="institucion" name="institucion" class="text">
                    <span class="error">Ingresa Tu Institución</span>
                </div>
                <div>
                    <label>Correo</label>
                    <input type="text" id="correo" name="correo" class="text">
                    <span class="error">Ingresa Tu Correo</span>
                </div> 
                <div>
                     <label>Seminario</label>
                     <p>Elige la ciudad donde participarás</p>

                     <span>
                        <input type="radio" name="ciudad" id="input_santiago"  value="Santiago" checked>
                        <label for="input_santiago">Santiago</label>
                     </span>
                    <!-- <span>
                        <input type="radio" name="ciudad" id="input_talca" value="Talca" >
                        <label for="input_talca">Talca</label>
                     </span>
                    <span>
                        <input type="radio" name="ciudad" id="input_temuco" value="Temuco" >
                        <label for="input_temuco">Temuco</label>
                     </span> -->
                </div>
                <a href="#" id="btn-inscripcion" class="btn btn-primary">Confirmar</a>
            </form>
            <div id="content-msj-exito">
                <div class="bien">
                    Gracias. Tu inscripción ha sido realizada con éxito.
                </div>
                <div class="error">
                    Ha ocurrido un error, inténtenlo más tarde.
                </div>
            </div>
          </div>
    </div> 
<?php get_footer('landing'); ?>
