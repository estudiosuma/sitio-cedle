<?php 
/*Template Name: Home*/
get_header(); 

?>
            <!-- CONTENIDO PRINCIPAL-->
            <div class="col-md-9">
            <style type="text/css">
                .section-1 h1 {
                    font-size: <?php echo get_field('tamano_de_letra', 'option');?>px;
                    width: 710px;
                    color: <?php echo get_field('color_de_letra', 'option');?>;
                }
                .section-1 p {
                    color: <?php echo get_field('color_de_letra', 'option');?>;
                }
            </style> 
                <!-- SECCION HOME -->
                <!-- Información extraida desde option home -->
                <div class="section-1">
                    <div class="row">
                        <img src="<?php echo get_field('foto_destacada', 'option'); ?>" class="fondo">
                        <div class="txt">
                            <h1><?php echo get_field('titulo', 'option'); ?></h1>
                            <p><?php echo get_field('bajada', 'option'); ?></p>
                            <a href="<?php echo get_field('url_del_boton', 'option'); ?>" title="<?php echo get_field('titulo', 'option'); ?>" class="btn btn-primary"><?php echo get_field('texto_del_boton', 'option'); ?></a> 
                        </div> 
                        <img src="<?php bloginfo('template_url');?>/img/home-m.gif" class="icn_scroll">
                    </div>
                </div>
                <!-- FIN SECCION HOME -->
                <!-- SECCION 13 -->
                <div class="section-13">
                    <div class="row">
                        <div class="col-md-12">
                        <?php 
                        $x=0;
                            if( have_rows('patrocinadores','option') ):
                                while( have_rows('patrocinadores','option') ): the_row();
                                    $x++;
                                    switch($x){
                                        case '3': $class_centrado='mediano';break;
                                        case '4': $class_centrado='subir';break;
                                        case '5': $class_centrado='corto';break;
                                    }
                                    echo '<div class="logo-patrocinador '.$class_centrado.'">';
                                    if (get_sub_field('url_patrocinador')) {
                                        echo '<a href="'.get_sub_field('url_patrocinador').'"><img class="img-responsive" src="'.get_sub_field('imagen_patrocinador').'"></a>';
                                    }else{
                                        echo '<img class="img-responsive" src="'.get_sub_field('imagen_patrocinador').'">';
                                    }
                                    echo '</div>';
                                    
                                endwhile;
                            endif;
                        ?>
                        </div>
                    </div>
                </div>
                <!-- FIN SECCION 13 -->
                <!-- SECCION 2 -->
                <div class="section-2">
                    <div class="row">
                        <div class="top">
                            <img src="<?php bloginfo('template_url');?>/img/icn_comillas.png">
                            <div class="clearfix"></div>
                            <h2><?php echo get_field('texto_vision_y_mision', 'option'); ?></h2>
                        </div>
                    </div>
                    <a href="/lineas-de-trabajo/" class="btn-mas">Ver Más</a>
                    <div class="row linea-tiempo dark hidden-xs hidden-sm" data-mcs-theme="minimal-dark">
                        <?php
                        query_posts(array('showposts' => 4, 'post_type' => 'lineas_de_trabajo', 'order'=> 'DESC', 'orderby' => 'order'));
                        if ( have_posts() ):
                            $x=0;
                            $i=0;
                            while (have_posts()) :the_post(); 
                                $x++;
                                echo '
                                    <div class="col-md-5 col-md-offset-1 item item-'.$x.'">
                                        <h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>
                                        <p>'.get_field('texto_resumido').'</p>
                                    </div>';
                                if ($x==2 && $i==0) {
                                    $x=0;
                                    $i=1;
                                    echo '<div class="clearfix"></div>';
                                }

                            endwhile;
                        endif;
                         ?>
                    </div>
                </div>
                <!-- FIN SECCION 2 -->
                <!-- SECCION 3 -->
                <div class="section-3">
                    <div class="row">
                        <h2 class="col-md-12">ACTUALIDAD</h2>
                         <div class="clearfix"></div>
                        <div class="scroll dark" data-mcs-theme="minimal-dark">
                            <?php
                            query_posts(array('showposts' => 6, 'post_type' => array('recomendado','columna'), 'order'=> 'DESC', 'orderby' => 'order'));
                            if ( have_posts() ):
                                $x=0;
                                $i=0;
                                echo '<div class="col-md-4">';
                                while (have_posts()) :the_post(); 
                               $taxonomy="columna_columnista";
                               $taxonomy2="autor_normativa";
                                    $x++;
                                    $class_box_actualidad='';
                                    $img_actualidad='';
                                    $span_actualidad='';
                                    switch ($post->post_type) {
                                        case 'columna':$aredireccion='<a href="'.get_permalink().'">'.get_the_title().'</a>'; $class_box_actualidad='box-columna';$terms = wp_get_post_terms( $post->ID, $taxonomy );if (get_field('foto_de_columnista',$taxonomy.'_'.$terms[0]->term_id)) { $img_actualidad='<a href="'.get_permalink().'"><img src="'.get_field('foto_de_columnista',$taxonomy.'_'.$terms[0]->term_id).'"></a>';};$span_actualidad='<span>Columna de: <strong>'.$terms[0]->name.'</strong></span>'; break;
                                        case 'recomendado':$aredireccion='<a target="_blank" href="'.get_permalink().'">'.get_the_title().'</a>'; $class_box_actualidad='box-recomienda';if (get_field('imagen_home')) {$img_actualidad='<a href="'.get_permalink().'"><img src="'.get_field('imagen_home').'"></a>';};$span_actualidad='<span>CEDLE Recomienda</span>'; break;
                                        case 'entrevista':$aredireccion='<a href="'.get_permalink().'">'.get_the_title().'</a>'; $class_box_actualidad='box-entrevista';if (get_field('imagen_home')) {$img_actualidad='<a href="'.get_permalink().'"><img src="'.get_field('imagen_home').'"></a>';};$span_actualidad='<span>Entrevista a : <strong>'.get_field('nombre_entrevistado').'</strong></span>'; break;
                                        case 'infografia':$aredireccion='<a href="'.get_permalink().'">'.get_the_title().'</a>'; $class_box_actualidad='box-infografia';$img_actualidad='<a href="'.get_permalink().'"><img src="'.get_bloginfo('template_url').'/img/icn_infografia.png"></a>';$span_actualidad='<span>Infografia</span>'; break;
                                        case 'columna':$aredireccion='<a href="'.get_permalink().'">'.get_the_title().'</a>'; $class_box_actualidad='box-columna';$terms = wp_get_post_terms( $post->ID, $taxonomy2 );if (get_field('foto_de_autor',$taxonomy2.'_'.$terms[0]->term_id)) { $img_actualidad='<a href="'.get_permalink().'"><img src="'.get_field('foto_de_autor',$taxonomy.'_'.$terms[0]->term_id).'"></a>';};$span_actualidad='<span>Normativa de: <strong>'.$terms[0]->name.'</strong></span>'; break;
                                    }
                                    echo '
                                        <div class="box '.$class_box_actualidad.'">';
                                    echo '  
                                            '.$img_actualidad.'
                                            '.$span_actualidad.'  
                                            <h3>'.$aredireccion.'</h3>
                                        </div>';
                                    if ($x==2 && $i<2) {
                                        $x=0;
                                        $i++;
                                        echo '</div>
                                            <div class="col-md-4 hidden-xs hidden-sm">';
                                    }
                                endwhile;
                                echo '</div>';
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <!-- FIN SECCION 3 -->
                <!-- SECCION 4 -->
                <div class="section-4">
                    <div class="row"> 
                        <h2 class="col-md-12">Noticias y Prensa</h2>
                        <div class="clearfix"></div>
                        <div class="scroll dark" data-mcs-theme="minimal-dark">
                            <?php
                            query_posts(array('showposts' => 6, 'post_type' => array('noticias','prensa'), 'order'=> 'DESC', 'orderby' => 'order'));
                            if ( have_posts() ):
                                $x=0;
                                $i=0;
                                echo '<div class="col-md-4">';
                                while (have_posts()) :the_post(); 
                                    $x++;
                                    echo '
                                        <div class="box-noticia">';
                                    if (get_field('imagen_home')) {
                                        echo '
                                        <a href="'.get_permalink().'"><img src="'.get_field('imagen_home').'"></a>';
                                    }
                                    echo '        
                                            <span>'.get_the_date('d/m').'</span>
                                            <h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>
                                            <p>'.get_field('texto_resumido').'</p>
                                            <div>'.do_shortcode( '[addtoany url="'.get_the_permalink().'" title="'.get_the_title().'" ]' ).'</div>
                                            <a href="'.get_the_permalink().'" class="ver-mas">Ver más</a>
                                        </div>';
                                    if ($x==2 && $i<2) {
                                        $x=0;
                                        $i++;
                                        echo '</div>
                                            <div class="col-md-4 hidden-xs hidden-sm">';
                                    }
                                endwhile;
                                echo '</div>';
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <!-- FIN SECCION 4 -->
                <!-- SECCION 5 -->
                <div class="section-5  hidden-xs hidden-sm">
                    <h2><?php echo get_field('titulo_de_la_seccion', 'option'); ?></h2>
                    <h3><span class="element"><?php
                    $x=0;
                    if( have_rows('texto_animado','option') ):
                        while( have_rows('texto_animado','option') ): the_row();
                            if ($x!=0) {
                                echo '-';
                            }
                            echo get_sub_field('palabra_animada');
                            $x++;
                        endwhile;
                    endif;
                    ?></span></h3>
                    <?php echo get_field('descripcion_de_la_seccion', 'option'); ?>
                    <form>
                        <input id="correo-newsletter-index" placeholder="Ingresa tu correo aquí" name="correo" type="text" class="text">
                        <input id="btn-enviar-suscripcion" type="button" class="btn-enviar" value="SUSCRÍBEME">
                    </form>
                </div>
                <!-- FIN SECCION 5 -->
                <!-- SECCION 6 -->
                <div class="section-6">
                    <div class="row">
                        <h2 class="col-md-12">Publicaciones</h2> 
                        <?php
                        query_posts(array('showposts' => 1, 'post_type' => 'publicaciones', 'order'=> 'DESC', 'orderby' => 'order', 'tax_query' => array('relation' => 'AND',array(
                        'taxonomy' => 'publicaciones_categoria',
                        'field' => 'term_id',
                        'terms' => array(4),
                        'operator'  => 'IN'),
                        )   ));
                        if ( have_posts() ):
                            $taxonomy="publicaciones_categoria";
                            while (have_posts()) :the_post();
                                $terms = wp_get_post_terms( $post->ID, $taxonomy );
                                // pr($terms);
                                foreach ($terms as $value) {
                                    // echo $value['term_id'];
                                    if ($value->term_id!=4) {
                                        $id_taxonomy_select = $value->term_id;
                                    }
                                }
                                $id_post_no_repetir = $post->ID;
                                echo '  <div class="col-md-4">
                                            <div class="box-publicaciones single">
                                                '.img_taxonomy_publicaciones($id_taxonomy_select).'
                                                <h3><a href="'.get_permalink().'">'.cortar_palabras(get_the_title(),110).'</a>. '.get_field('texto_resumido').'</h3> 
                                            </div> 
                                        </div>';
                            endwhile;
                            wp_reset_query();
                            query_posts(array('showposts' => 4, 'post_type' => 'publicaciones','post__not_in' => array($id_post_no_repetir) ,'order'=> 'DESC', 'orderby' => 'order'));
                            if ( have_posts() ):
                                $x=0;
                                $taxonomy="publicaciones_categoria";
                                echo '<div class="col-md-4  hidden-xs hidden-sm">';
                                while (have_posts()) :the_post();
                                    $x++;
                                    $id_taxonomy_select=0;
                                    $terms = wp_get_post_terms( $post->ID, $taxonomy );
                                    foreach ($terms as $value) {
                                        if ($value->term_id!=4) {
                                            $id_taxonomy_select = $value->term_id;
                                        }
                                    }
                                    $id_post_no_repetir = $post->ID;
                                    echo '
                                            <div class="box-publicaciones">
                                                '.img_taxonomy_publicaciones($id_taxonomy_select).'
                                                <h3><a href="'.get_permalink().'">'.cortar_palabras(get_the_title(),110).'</a></h3> 
                                            </div>';
                                    if ($x==2) {
                                        echo '</div>
                                            <div class="col-md-4 hidden-xs hidden-sm">';
                                    }
                                endwhile;
                                echo '</div>';
                            endif;
                        else:
                            query_posts(array('showposts' => 6, 'post_type' => 'publicaciones','order'=> 'DESC', 'orderby' => 'order'));
                            if ( have_posts() ):
                                $x=0;
                                $taxonomy="publicaciones_categoria";
                                echo '<div class="col-md-4">';
                                while (have_posts()) :the_post();
                                    $x++;
                                    $id_taxonomy_select=0;
                                    $terms = wp_get_post_terms( $post->ID, $taxonomy );
                                    foreach ($terms as $value) {
                                        if ($value->term_id!=4) {
                                            $id_taxonomy_select = $value->term_id;
                                        }
                                    }
                                    $id_post_no_repetir = $post->ID;
                                    echo '
                                            <div class="box-publicaciones">
                                                '.img_taxonomy_publicaciones($id_taxonomy_select).'
                                                <h3><a href="'.get_permalink().'">'.cortar_palabras(get_the_title(),110).'</a></h3> 
                                            </div>';
                                    if ($x==2 && $i<2) {
                                        $x=0;
                                        $i++;
                                        echo '  </div>
                                                <div class="col-md-4 hidden-xs hidden-sm">';
                                    }
                                endwhile;
                                echo '</div>';
                            endif;
                        endif;
                            ?>
                    </div> 
                    <!-- <a href="#" class="btn-mas">Ver Más</a> -->
                </div>
                <!-- FIN SECCION 6 -->
                <!-- SECCION 7 -->
                <div class="section-7 hidden-xs hidden-sm" >
                    <div class="row">  
                        <div class="col-md-5 left ">
                            <div class="contenido_sucursal">
                                <?php
                                    query_posts(array('showposts' => 1, 'post_type' => 'sucursal', 'p'=>'57', 'order'=> 'DESC', 'orderby' => 'order'));
                                    if ( have_posts() ):
                                        while (have_posts()) :the_post(); 
                                            echo '  <img src="'.get_field('imagen_home').'">
                                                    <div class="txt">
                                                        <h3>'.get_the_title().'</h3>
                                                        <p>'.get_field('bajada').'</p>
                                                        <div>
                                                            <strong>Teléfono</strong>
                                                            <span>'.get_field('telefono').'</span>
                                                        </div>
                                                        <div>
                                                            <strong>Dirección</strong>
                                                            <span>'.get_field('direccion').'</span>
                                                        </div>
                                                        <div>
                                                            <strong>Horario de atención</strong>
                                                            <span>'.get_field('horario').'</span>
                                                        </div>
                                                    </div>';
                                        endwhile;
                                    endif;
                                ?>
                            </div>
                        </div>
                        <div class="col-md-7 mapa">
                            <a href="#" class="btn_mapa mapa_california"> </a>
                            <a href="#" class="btn_mapa mapa_santiago active"> </a>
                            <a href="#" class="btn_mapa mapa_talca"> </a>
                            <a href="#" class="btn_mapa mapa_temuco"> </a> 
                        </div>
                    </div>
                </div>
                <!-- FIN SECCION 7 -->

                <?php 
                $dias_mes = diasMes();
                $inicio_de_mes= date('Ym').'01';
                // echo $inicio_de_mes.'-'.date('Ym').$dias_mes;    

                query_posts(array('showposts' => 100, 'post_type' => 'evento', 'order'=> 'DESC', 'orderby' => 'order','meta_query' => array(
                                'relation' => 'AND',
                                array(
                                    'key' => 'fecha_del_evento',
                                    'value' => $inicio_de_mes,
                                    'compare' =>'>='
                                     ),
                                array(
                                    'key' => 'fecha_del_evento',
                                    'value' => date('Ym').$dias_mes,
                                    'compare' =>'<='
                                     )
                               )));
                if ( have_posts() ):
                    while (have_posts()) :the_post(); 

                        $date = get_field('fecha_del_evento');
                        // echo $date;
                        $fecha_del_evento=date('j',strtotime($date));
                        $html[$fecha_del_evento]['id']=$post->ID;
                        $html_img='';
                        if (get_field('imagen_home')) {
                            $html_img= '<img src="'.get_field('imagen_home').'">';
                        }
                        
                        $html[$fecha_del_evento]['html']='  
                                    <div class="txt">
                                        <small>'.date('d',strtotime($date)).' '.mes_espanol(date('n',strtotime($date))).', '.date('Y',strtotime($date)).'</small>
                                        <span>Streaming</span>
                                        <h3>'.get_the_title().'</h3>
                                        <p>'.get_field('descripcion_del_evento').'</p>
                                        '.$html_img.'
                                        <a href="/agenda/" class="ver_mas">Ver más</a> 

                                    </div>';
                    endwhile;
                endif; ?>

                <!-- SECCION 8 -->
                <div class="section-8 hidden-xs hidden-sm"> 
                    <div class="row">
                        <div class="col-md-5 calendario">
                            <div class="row">
                                <?php 
                                $x=1;
                                $z=0;
                                $y=0;
                                for ($i=1; $i <= $dias_mes; $i++) { 
                                    switch ($x) {
                                        case '1':$numero='uno';$x++;break;
                                        case '2':$numero='dos';$x++;break;
                                        case '3':$numero='tres';$x=1;break;
                                    }
                                    if ($html[$i]['id']) {
                                        echo '<a class="col-md-2 activo '.$numero.'"  data-evento="'.$html[$i]['id'].'" href="#">'.$i.'</a>';
                                        if ($z==0) {
                                            $z=$i;
                                        }
                                    }else{
                                        echo '<div class="col-md-2 '.$numero.'"  href="#">'.$i.'</div>';
                                    }
                                    $y=$y+2;
                                }
                                $tamano_barra_evento=$y%12;
                                $tamano_barra_evento=12-$tamano_barra_evento;
                                // echo $tamano_barra_evento;
                                if ($tamano_barra_evento<=4) {
                                    $tamano_barra_evento=12;
                                }
                                ?>
                                <div class="col-md-<?php echo $tamano_barra_evento;?> btn-selecciona" href="#">Selecciona un evento para visualizar  ></div> 
                            </div>
                        </div>
                        <div class="col-md-7 item-evento">
                            <!-- aca cargar nuevo evento -->
                            <?php if($html[$z]['id']){ ?>
                                <?php echo $html[$z]['html']; ?>
                            <?php }else{ ?>
                                <style type="text/css">
                                .item-evento {
                                    background: #3c3c3c none repeat scroll 0 0;
                                    height: 470px;
                                    margin-left: -1px;
                                    padding: 0;
                                }
                                .col-md-7.item-evento > p {
                                    color: #fff;
                                    font-size: 16px;
                                    margin-top: 200px;
                                    text-align: center;
                                }
                                </style>
                                <p>No se encuentran eventos para el mes de <?php echo strtolower(mes_espanol(date('m'))); ?>.</p>
                            <?php } ?>
                            
                            <!--  fin  -->  
                        </div>
                    </div>
                    
                </div>
                <!-- FIN SECCION 8 -->


                <?php if(get_field('activar_seccion_nombre', 'option')): ?>
                <!-- SECCION 9 -->
                <div class="section-9"> 
                    <div class="row">
                     <?php 
                        $x=0;
                        if( have_rows('secciones_multimedia','option') ):
                            $rows_count = get_field('secciones_multimedia','option');
                            $rows_count=count($rows_count);
                            // pr( $rows_count);
                            while( have_rows('secciones_multimedia','option') ): the_row();
                                if ($rows_count==1): ?>
                                    <div class="col-md-12">
                                        <div class="box single">
                                            <img src="<?php bloginfo('template_url');?>/img/icn_<?php echo get_sub_field('tipo_de_multimedia'); ?>.png"> 
                                            <h3><a href="<?php echo get_sub_field('url_multimedia'); ?>"><?php echo get_sub_field('titulo_del_multimedia'); ?></a></h3> 
                                            <a href="<?php echo get_sub_field('url_multimedia'); ?>" class="vermas">Ver <?php echo ucwords(get_sub_field('tipo_de_multimedia')); ?></a>
                                        </div>
                                    </div>
                                <?php
                                elseif($rows_count==2):?>
                                    <div class="col-md-6">
                                        <div class="box single">
                                            <img src="<?php bloginfo('template_url');?>/img/icn_<?php echo get_sub_field('tipo_de_multimedia'); ?>.png"> 
                                            <h3><a href="<?php echo get_sub_field('url_multimedia'); ?>"><?php echo get_sub_field('titulo_del_multimedia'); ?></a></h3> 
                                            <a href="<?php echo get_sub_field('url_multimedia'); ?>" class="vermas">Ver <?php echo ucwords(get_sub_field('tipo_de_multimedia')); ?></a>
                                        </div>
                                    </div>
                                    <?php
                                elseif($rows_count==3):
                                    if ($x==0): ?>
                                    <div class="col-md-6">
                                        <div class="box single">
                                            <img src="<?php bloginfo('template_url');?>/img/icn_<?php echo get_sub_field('tipo_de_multimedia'); ?>.png"> 
                                            <h3><a href="<?php echo get_sub_field('url_multimedia'); ?>"><?php echo get_sub_field('titulo_del_multimedia'); ?></a></h3> 
                                            <a href="<?php echo get_sub_field('url_multimedia'); ?>" class="vermas">Ver <?php echo ucwords(get_sub_field('tipo_de_multimedia')); ?></a>
                                        </div>
                                    </div>
                                        <?php
                                    elseif($x==1): ?>
                                    <div class="col-md-6 hidden-xs hidden-sm">
                                        <div class="box normal">
                                            <img src="<?php bloginfo('template_url');?>/img/icn_<?php echo get_sub_field('tipo_de_multimedia'); ?>.png"> 
                                            <h3><a href="<?php echo get_sub_field('url_multimedia'); ?>"><?php echo get_sub_field('titulo_del_multimedia'); ?></a></h3> 
                                            <a href="<?php echo get_sub_field('url_multimedia'); ?>" class="vermas">Ver <?php echo ucwords(get_sub_field('tipo_de_multimedia')); ?></a>
                                        </div>
                                        <?php
                                        else: ?>
                                        <div class="box normal">
                                            <img src="<?php bloginfo('template_url');?>/img/icn_<?php echo get_sub_field('tipo_de_multimedia'); ?>.png"> 
                                            <h3><a href="<?php echo get_sub_field('url_multimedia'); ?>"><?php echo get_sub_field('titulo_del_multimedia'); ?></a></h3> 
                                            <a href="<?php echo get_sub_field('url_multimedia'); ?>" class="vermas">Ver <?php echo ucwords(get_sub_field('tipo_de_multimedia')); ?></a>
                                        </div>
                                    </div>
                                        <?php
                                    endif;
                                endif;
                                $x++;
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
                <!-- FIN SECCION 9 -->
                <?php endif; ?>


                <?php if( get_field('activar_banner', 'option')): ?>
                <!-- SECCION 10 -->
                <div class="section-10">
                    <div class="row">
                        <?php 
                            query_posts(array('showposts' => get_field('cantidad_banner', 'option'), 'post_type' => 'banner', 'order'=> 'DESC', 'orderby' => 'order'));
                            if ( have_posts() ):
                                $x=0;
                                
                                while (have_posts()) :the_post(); 
                                    if ($x>=2) {
                                        echo '<div class="col-md-4 hidden-xs hidden-sm">';
                                    }else{
                                        echo '<div class="col-md-4">';
                                    }
                                    if (get_field('url_banner')) {
                                        echo '<a href="'.get_field('url_banner').'"><img src="'.get_field('imagen_home').'"></a>';
                                    }else{
                                        echo '<img src="'.get_field('imagen_home').'">';
                                    }
                                    echo '</div>';
                                    $x++;
                                endwhile;
                            endif;
                        ?>
                    </div>
                </div>
                <!-- FIN SECCION 10 -->
                
                <?php endif; ?>


                <!-- SECCION 11 -->
                <div class="section-11">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <h3>Patrocinadores</h3> -->
                        </div>
                        <?php 
                        $x=0;
                            if( have_rows('patrocinadores','option') ):
                                while( have_rows('patrocinadores','option') ): the_row();
                                    if($x==3){
                                        // $class_centrado='col-md-offset-2';
                                    }else{
                                        // $class_centrado='';
                                    }
                                    echo '<div class="col-md-4 logo-patrocinador '.$class_centrado.'">';
                                    if (get_sub_field('url_patrocinador')) {
                                        echo '<a href="'.get_sub_field('url_patrocinador').'"><img src="'.get_sub_field('imagen_patrocinador').'"></a>';
                                    }else{
                                        echo '<img src="'.get_sub_field('imagen_patrocinador').'">';
                                    }
                                    echo '</div>';
                                    $x++;
                                endwhile;
                            endif;
                        ?>
                        <!-- <div class="col-md-4">
                            <img src="<?php bloginfo('template_url');?>/img/logo_udp.png">
                        </div>
                        <div class="col-md-4">
                             <img src="<?php bloginfo('template_url');?>/img/logo_udp.png">
                        </div>
                        <div class="col-md-4">
                             <img src="<?php bloginfo('template_url');?>/img/logo_udp.png">
                        </div> -->
                        <div class="col-md-6 text-left">
                            <p><?php  echo get_field('texto_informacion_legal', 'option'); ?></p>
                        </div>
                        <div class="col-md-6 text-right">
                        </div>
                    </div>
                </div>
                <!-- FIN SECCION 11 -->



                <!-- SECCION SUCURSALES -->
                <div class="section-hiden" style="display: none;">
                    <div id="div-contenedor-mapa_california">
                    <?php
                        query_posts(array('showposts' => 1, 'post_type' => 'sucursal', 'p'=>'55', 'order'=> 'DESC', 'orderby' => 'order'));
                        if ( have_posts() ):
                            while (have_posts()) :the_post(); 
                                echo '  <img src="'.get_field('imagen_home').'">
                                        <div class="txt">
                                            <h3>'.get_the_title().'</h3>
                                            <p>'.get_field('bajada').'</p>';
                                if (get_field('telefono')) :
                                echo '      <div>
                                                <strong>Teléfono</strong>
                                                <span>'.get_field('telefono').'</span>
                                            </div>';
                                endif;
                                if (get_field('direccion')) :
                                echo '      <div>
                                                <strong>Dirección</strong>
                                                <span>'.get_field('direccion').'</span>
                                            </div>';
                                endif;
                                if (get_field('horario')) :
                                echo '      <div>
                                                <strong>Horario de atención</strong>
                                                <span>'.get_field('horario').'</span>
                                            </div>';
                                endif;

                                echo '      </div>';
                            endwhile;
                        endif;
                    ?>
                    </div>
                    <div id="div-contenedor-mapa_santiago">
                        <?php
                        query_posts(array('showposts' => 1, 'post_type' => 'sucursal', 'p'=>'57', 'order'=> 'DESC', 'orderby' => 'order'));
                        if ( have_posts() ):
                            while (have_posts()) :the_post(); 
                                echo '  <img src="'.get_field('imagen_home').'">
                                        <div class="txt">
                                            <h3>'.get_the_title().'</h3>
                                            <p>'.get_field('bajada').'</p>';
                                if (get_field('telefono')) :
                                echo '      <div>
                                                <strong>Teléfono</strong>
                                                <span>'.get_field('telefono').'</span>
                                            </div>';
                                endif;
                                if (get_field('direccion')) :
                                echo '      <div>
                                                <strong>Dirección</strong>
                                                <span>'.get_field('direccion').'</span>
                                            </div>';
                                endif;
                                if (get_field('horario')) :
                                echo '      <div>
                                                <strong>Horario de atención</strong>
                                                <span>'.get_field('horario').'</span>
                                            </div>';
                                endif;

                                echo '      </div>';
                            endwhile;
                        endif;
                    ?>
                    </div>
                    <div id="div-contenedor-mapa_talca">
                        <?php
                        query_posts(array('showposts' => 1, 'post_type' => 'sucursal', 'p'=>'58', 'order'=> 'DESC', 'orderby' => 'order'));
                        if ( have_posts() ):
                            while (have_posts()) :the_post(); 
                                echo '  <img src="'.get_field('imagen_home').'">
                                        <div class="txt">
                                            <h3>'.get_the_title().'</h3>
                                            <p>'.get_field('bajada').'</p>';
                                if (get_field('telefono')) :
                                echo '      <div>
                                                <strong>Teléfono</strong>
                                                <span>'.get_field('telefono').'</span>
                                            </div>';
                                endif;
                                if (get_field('direccion')) :
                                echo '      <div>
                                                <strong>Dirección</strong>
                                                <span>'.get_field('direccion').'</span>
                                            </div>';
                                endif;
                                if (get_field('horario')) :
                                echo '      <div>
                                                <strong>Horario de atención</strong>
                                                <span>'.get_field('horario').'</span>
                                            </div>';
                                endif;

                                echo '      </div>';
                            endwhile;
                        endif;
                    ?>
                    </div>
                    <div id="div-contenedor-mapa_temuco">
                        <?php
                        query_posts(array('showposts' => 1, 'post_type' => 'sucursal', 'p'=>'59', 'order'=> 'DESC', 'orderby' => 'order'));
                        if ( have_posts() ):
                            while (have_posts()) :the_post(); 
                                echo '  <img src="'.get_field('imagen_home').'">
                                        <div class="txt">
                                            <h3>'.get_the_title().'</h3>
                                            <p>'.get_field('bajada').'</p>';
                                if (get_field('telefono')) :
                                echo '      <div>
                                                <strong>Teléfono</strong>
                                                <span>'.get_field('telefono').'</span>
                                            </div>';
                                endif;
                                if (get_field('direccion')) :
                                echo '      <div>
                                                <strong>Dirección</strong>
                                                <span>'.get_field('direccion').'</span>
                                            </div>';
                                endif;
                                if (get_field('horario')) :
                                echo '      <div>
                                                <strong>Horario de atención</strong>
                                                <span>'.get_field('horario').'</span>
                                            </div>';
                                endif;

                                echo '      </div>';
                            endwhile;
                        endif;
                    ?>
                    </div>
                </div>  
                <!-- FIN SECCION SUCURSALES -->
                <!-- SECCION EVENTOS -->
                <div class="section-hiden" style="display: none;">
                    <?php
                        foreach ($html as $key => $value) {
                            echo '<div id="evento-'.$value['id'].'" >'.$value['html'].'</div>';
                        }
                    ?>

                </div>
                
                <!-- Modal -->
                <div class="modal fade" id="modal-sin-cupos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-body">
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h1>Seminario Internacional <strong>CEDLE</strong></h1>
                                <p>Por éxito de inscripciones, los cupos para nuestros Seminarios se encuentran cerrados.</p>
                                <p>Podrás seguir el Seminario Internacional en vivo vía streaming el día viernes 13 de mayo desde las 9:30 hrs en <strong>cedle.cl</strong></p>
                                <p>Muchas gracias.</p>
                            </div>
                        </div>
                         
                      </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-cerrar" data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>

                

<?php get_footer(); ?>
    <a class="btn" data-toggle="modal" href="#modal-sin-cupos" style="display:none;">Modal Sin Cupos</a>
    <script type="text/javascript">
        $( window ).load(function() {
            $('.item-evento .txt').addClass("scroll2");
            $(".scroll2").mCustomScrollbar({
                    scrollButtons:{enable:true},
                    theme:"light-thick",
                    scrollInertia:100,
                    scrollbarPosition:"inside"
                });
            // $('#modal-sin-cupos').modal('show');
        });
    </script>