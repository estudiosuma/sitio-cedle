<?php get_header(); 
$id = get_the_ID(); 
if(get_field('imagen_de_fondo')){ ?>
    <style type="text/css">
      .single_prensa {
          background: #efefef url("<?php echo get_field('imagen_de_fondo'); ?>") no-repeat scroll -1px top;
      }
    </style>
<?php } ?>
 <!-- CONTENIDO PRINCIPAL-->
            <div class="col-md-9">

                <!-- Contenido seccion -->
                <div class="section single_prensa single row">
                    <div class="col-xs-6 nav-left">
                        <a href="/noticias-y-prensa/"><i class="fa fa-arrow-left"></i> Noticias & prensa</a>
                    </div>
					<div class="col-xs-6 nav-right">
                    <?php $next_post = get_next_post(); ?>
		            <?php if ( is_a( $next_post, 'WP_Post' ) ) {  ?>
		                <a href="<?php echo get_permalink( $next_post->ID ); ?>">Prensa Siguiente <i class="fa fa-arrow-right"></i></a>
		            <?php } ?> 
		            </div> 
                    <div class="col-md-12">
                        <div class="text-center top"> 
                            <h1><?php the_title(); ?></h1>
                        </div>

                        <div class="padre_principal">
                            <div class="row principal  ">
                                <div class="col-md-12">
                                    <span><?php echo get_field('fecha'); ?></span>
                                    <?php echo get_field('texto'); ?>
                                </div> 
                            </div>
                            <div class="row fuente">
                                <div class="col-md-6 left">
                                    <p>Fuente: <strong><?php echo get_field('fuente'); ?></strong></p>
                                    <a target="_blank" href="<?php echo get_field('url_fuente'); ?>">Leer la noticia desde la fuente ></a>
                                </div>
                                <div class="col-md-6 right">
                                <?php
                                    $url=get_permalink( $id );
                                    echo do_shortcode( '[addtoany url="'.$url.'" title="'.get_the_title().'" ]' );?>
                                </div>
                            </div>

                            <div class="row secundario  ">
                                <?php 
                                disqus_embed('cedle');
                                    // $url=get_permalink( $id );
                                    // echo do_shortcode('[fbcomments url="'.$url.'" width="100%" count="off" num="3" countmsg="wonderful comments!"]'); ?>
                            </div>
                        </div>
                    </div>
                </div> 
<?php get_footer(); ?>