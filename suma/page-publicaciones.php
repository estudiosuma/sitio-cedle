<?php
/*Template Name: Publicaciones */
get_header(); ?>
            <!-- CONTENIDO PRINCIPAL-->
            <div class="col-md-9">

                <!-- Contenido seccion -->
                <div class="section section_actualidad  section_publicaciones "> 
                    <div class="col-xs-6 nav-left">
                        <a href="/index/"><i class="fa fa-arrow-left"></i> Home</a>
                    </div>
                    <div class="col-xs-6 nav-right">
                        <a href="/noticias-y-prensa/">Noticias & Prensa <i class="fa fa-arrow-right"></i></a>
                    </div>
                    <div class="col-md-12" >
                            <div class="text-center top">
                                <img src="<?php bloginfo('template_url');?>/img/icn_publicacion.png">
                                <h1>Publicaciones</h1>
                            </div> 
                            <!-- SECCION 3 -->
                            <div id="contenedor_cajas">
                            <nav class="filter-options row">
                                    <a href="#" data-group="Cuaderno" id="btn_cuaderno" > Cuadernos</a> 
                                    <a href="#" data-group="Artículos" id="btn_estudios"  class=" active">  Artículos</a> 
                                    <a href="#" data-group="Estudios" id="btn_informe"  > Estudios</a> 
                            </nav>
                            </div>
                            <div class="filtros row">
                                <div>
                                    <span class="txt">Filtrar por: </span>
                                    <select class="selectpicker" id="select-publicacion-ano" name="select-publicacion-ano">
                                        <option value="0" >Año</option> 
                                        <option value="2016" selected>2016</option> 
				                        <option value="2015">2015</option> 
				                        <option value="2014">2014</option> 
				                        <option value="2013">2013</option>
				                        <option value="2012">2012</option> 
				                        <option value="2011">2011</option> 
				                        <option value="2010">2010</option> 
                                    </select>

                                    <select class="selectpicker autor" id="select-publicacion-autor" name="select-publicacion-autor">
                                        <option value="0" >Autor</option>  
										<?php
				                        query_posts(array('showposts' => 1000, 'post_type' => 'equipo_cedle', 'order'=> 'ASC', 'orderby' => 'title' ));
				                        if ( have_posts() ):
				                            while (have_posts()) :the_post();
				                        		echo '<option value="'.$post->ID.'" >'.get_the_title().'</option>';
				                            endwhile;
				                        endif;
						                        ?>
                                    </select>

                                    <select class="selectpicker" id="select-publicacion-tema" name="select-publicacion-tema">
                                        <option value="0" >Tema</option>  
                                        <?php 
                                        $terms = get_terms( array(
																    'taxonomy' => 'publicaciones_tema',
																    'hide_empty' => false,
																) );
                                        // pr($terms);
                                        foreach ($terms as $key => $value) {
                                        	echo '<option value="'.$value->term_id.'" >'.$value->name.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="simbologia row">
                                <span>
                                    <img src="<?php bloginfo('template_url');?>/img/icn_simbologia_pdf.png">
                                    PDF
                                </span>
                                <span>
                                    <img src="<?php bloginfo('template_url');?>/img/icn_simbologia_doc.png">
                                    Documento
                                </span>
                                <span>
                                    <img src="<?php bloginfo('template_url');?>/img/icn_simbologia_audio.png">
                                    Audio
                                </span>
                                <span>
                                   <img src="<?php bloginfo('template_url');?>/img/icn_simbologia_video.png">
                                    Video
                                </span>
                            </div>

                            <div class="section-3">
                                <div class="row" >
                                    <div class="clearfix"></div>
                                    <div id="contenedor-de-publicaciones"> 
                                        <div class="col-md-12 test" id="grid2"> 
                                        <?php 
                                        query_posts(array('showposts' => 1000, 'post_type' => 'publicaciones','order'=> 'DESC', 'orderby' => 'order'));
			                            if ( have_posts() ):
			                                $x=0;
			                                $taxonomy="publicaciones_categoria";
			                                $taxonomy2="publicaciones_tipo";
			                                $taxonomy3="publicaciones_tema";
			                                while (have_posts()) :the_post();
			                                    $id_taxonomy_select=0;
			                                    $terms = wp_get_post_terms( $post->ID, $taxonomy );
			                                    foreach ($terms as $value) {
			                                        if ($value->term_id!=4) {
			                                            $id_taxonomy_select = $value;
			                                        }
			                                    }
			                                    $terms2 = wp_get_post_terms( $post->ID, $taxonomy2 );
			                                    $terms3 = wp_get_post_terms( $post->ID, $taxonomy3 );
			                                    $temas='';
			                                    foreach ($terms3 as $key => $value) {
			                                    	if ($key>0) {
			                                    		$temas.=', ';
			                                    	}
			                                    	$temas.= $value->name;
			                                    }
			                                    $id_post_no_repetir = $post->ID;
			                                    $post_object = get_field('autor');
                                                $autores='';
                                                if($post_object){
                                                    foreach ($post_object as $key => $value) {
                                                        if ($key>0) {
                                                            $autores.=', ';
                                                        }
                                                        $autores.= $value->post_title;
                                                    }
                                                }
                                                ?>
			                                <div class="col-md-4 items" data-groups='["<?php echo $id_taxonomy_select->name; ?>"]'>
                                                <div class="box-publicaciones">
                                                    <?php echo img_taxonomy_publicaciones($id_taxonomy_select->term_id); ?>
                                                    <!-- <h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>  -->
                                                    <h3><?php echo cortar_palabras(get_the_title(),90); ?></h3>
                                                    <div class="detalle">
                                                        <p><strong>Fecha:</strong> <?php echo get_field('anos_de_publicacion'); ?></p>
                                                        <p><strong>Autor:</strong> <?php echo cortar_palabras($autores,54);?></p>
                                                        <p><strong>Tema: </strong><?php echo $temas; ?></p>
                                                        <div class="descarga">
                                                            <?php echo img_taxonomy_publicaciones_tipo($terms2[0]->term_id); ?>
                                                            <a href="<?php echo get_the_permalink(); ?>">Ver</a> |
                                                            <a href="<?php echo get_field('archivo'); ?>">Descargar</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <?php
			                                    
			                                endwhile;
			                            endif;

                                        ?>
                                        </div>
                                        <div class="error-publiaciones">
                                            <p>No se encontraron publicaciones. Intenta seleccionado una categoría entre Cuadernos, Artículos o Estudios y filtrando según Año, Autor o Tema.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-cargar row">
                                    <div class="btn btn-primary"></div> 
                                </div>
                            </div>
                            <!-- FIN SECCION 3 -->
                        </div>
                    </div>
<?php get_footer(); ?>